package libreriaAlberto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Hex;

/**
 * Clase Crypto
 * 
 * @author cloud
 *
 */
public class Crypto {
	

	
	/**
	 * Reducce el tama�o del has y lo codifica con base64
	 * 
	 * @param mb
	 * @return
	 */
	public String base64Codificar(byte[] mb) {
		String entrada = String.copyValueOf(Hex.encodeHex(mb));
		String cadenaCodificada = Base64.getEncoder().encodeToString(entrada.getBytes());

		return cadenaCodificada;
	}

	/**
	 * Decodifica con base64
	 * 
	 * @param mb
	 * @return
	 */
	public String base64Decodificar(String cadenaCodificada) {

		byte[] bytesDecodificados = Base64.getDecoder().decode(cadenaCodificada);
		String cadenaDecodificada = new String(bytesDecodificados);
		return cadenaDecodificada;
	}

	/**
	 * Crea el has de la contrase�a y debuelve el valor del mismo
	 * 
	 * @param password
	 * @return
	 */
	public byte[] hasearContrasena(String password) {
		// Se crea el objeto MessageDigest
		MessageDigest md = null;
		// SHA-512
		try {
			md = MessageDigest.getInstance("SHA-512");
			md.update(password.getBytes());
			// Se realiza el Hashing
			byte[] mb = md.digest();
			return mb;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Crea una clave segura
	 * 
	 * @param clave
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public SecretKeySpec crearClave(String clave) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		byte[] claveEncriptacion = clave.getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-512");
		claveEncriptacion = sha.digest(claveEncriptacion);
		claveEncriptacion = Arrays.copyOf(claveEncriptacion, 16);
		SecretKeySpec secretKey = new SecretKeySpec(claveEncriptacion, "AES");

		return secretKey;
	}

	/**
	 * Genera la encriptacion
	 * 
	 * @param datos
	 * @param claveSecreta
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */

	public String encriptar(String datos, String claveSecreta)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException {
		SecretKeySpec secretKey = this.crearClave(claveSecreta);
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(1, secretKey);
		byte[] datosEncriptar = datos.getBytes("UTF-8");
		byte[] bytesEncriptados = cipher.doFinal(datosEncriptar);
		String encriptado = Base64.getEncoder().encodeToString(bytesEncriptados);
		return encriptado;
	}

	/**
	 * Genera la desencriptacion de los datos
	 * 
	 * @param datosEncriptados
	 * @param claveSecreta
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String desencriptar(String datosEncriptados, String claveSecreta)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException {

		SecretKeySpec secretKey = this.crearClave(claveSecreta);
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(2, secretKey);
		byte[] bytesEncriptados = Base64.getDecoder().decode(datosEncriptados);
		byte[] datosDesencriptados = cipher.doFinal(bytesEncriptados);
		String datos = new String(datosDesencriptados);
		return datos;
	}

}