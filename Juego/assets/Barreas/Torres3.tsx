<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.3" name="Torres3" tilewidth="32" tileheight="32" tilecount="30" columns="6">
 <image source="towers_walls_grass_light_2.png" width="192" height="160"/>
 <tile id="0" type="torre"/>
 <tile id="1" type="torre"/>
 <tile id="2" type="torre"/>
 <tile id="3" type="torre"/>
 <tile id="4" type="torre"/>
 <tile id="5" type="torre"/>
 <tile id="6" type="torre"/>
 <tile id="7" type="torre"/>
 <tile id="8" type="torre"/>
 <tile id="9" type="torre"/>
 <tile id="10" type="torre"/>
 <tile id="11" type="torre"/>
 <tile id="12" type="torre"/>
 <tile id="13" type="torre"/>
 <tile id="14" type="torre"/>
 <tile id="15" type="torre"/>
 <tile id="16" type="torre"/>
 <tile id="17" type="torre"/>
 <tile id="18" type="torre"/>
 <tile id="19" type="torre"/>
 <tile id="20" type="torre"/>
 <tile id="21" type="torre"/>
 <tile id="22" type="torre"/>
 <tile id="23" type="torre"/>
 <tile id="24" type="barrera"/>
 <tile id="25" type="barrera"/>
 <tile id="26" type="barrera"/>
 <tile id="27" type="barrera"/>
 <tile id="28" type="barrera"/>
 <tile id="29" type="barrera"/>
</tileset>
