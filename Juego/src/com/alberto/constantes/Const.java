package com.alberto.constantes;

/**
 * Clase Const, guarda las constantes que se usaran en el juego
 * @author cloud
 *
 */
public class Const {
	// Atributos
	public static final int VIDA = 1000;
	public static final int COINT = 0;
	public static final int anchura = 800;
	public static final int altura = 608;
	public static final int PRICE_T_1 = 5;
	public static final int PRICE_T_2 = 7;
	public static final int PRICE_T_3 = 9;
	public static final String[] TORRE_1_LVL_1_STOP = {"assets/torretasUser/torreta1/nivel1/tile000.png"};
	public static final String[] TORRE_1_LVL_1_MOV = {"assets/torretasUser/torreta1/nivel1/tile000.png",
			"assets/torretasUser/torreta1/nivel1/tile001.png",
			"assets/torretasUser/torreta1/nivel1/tile002.png",
			"assets/torretasUser/torreta1/nivel1/tile003.png",
			"assets/torretasUser/torreta1/nivel1/tile004.png",
			"assets/torretasUser/torreta1/nivel1/tile005.png",
			"assets/torretasUser/torreta1/nivel1/tile006.png",
			"assets/torretasUser/torreta1/nivel1/tile007.png"};
	public static final String[] TORRE_1_LVL_2_STOP = {"assets/torretasUser/torreta1/nivel2/tile000.png"};
	public static final String[] TORRE_1_LVL_2_MOV = {"assets/torretasUser/torreta1/nivel2/tile000.png",
			"assets/torretasUser/torreta1/nivel2/tile001.png",
			"assets/torretasUser/torreta1/nivel2/tile002.png",
			"assets/torretasUser/torreta1/nivel2/tile003.png",
			"assets/torretasUser/torreta1/nivel2/tile004.png",
			"assets/torretasUser/torreta1/nivel2/tile005.png",
			"assets/torretasUser/torreta1/nivel2/tile006.png",
			"assets/torretasUser/torreta1/nivel2/tile007.png"};
	public static final String[] TORRE_1_LVL_3_STOP = {"assets/torretasUser/torreta1/nivel3/tile000.png"};
	public static final String[] TORRE_1_LVL_3_MOV = {"assets/torretasUser/torreta1/nivel3/tile000.png",
			"assets/torretasUser/torreta1/nivel3/tile001.png",
			"assets/torretasUser/torreta1/nivel3/tile002.png",
			"assets/torretasUser/torreta1/nivel3/tile003.png",
			"assets/torretasUser/torreta1/nivel3/tile004.png",
			"assets/torretasUser/torreta1/nivel3/tile005.png",
			"assets/torretasUser/torreta1/nivel3/tile006.png",
			"assets/torretasUser/torreta1/nivel3/tile007.png"};
	public static final String[] TORRE_2_LVL_1_STOP = {"assets/torretasUser/torreta2/nivel1/tile000.png"};
	public static final String[] TORRE_2_LVL_1_MOV = {"assets/torretasUser/torreta2/nivel1/tile000.png",
			"assets/torretasUser/torreta2/nivel1/tile001.png",
			"assets/torretasUser/torreta2/nivel1/tile002.png",
			"assets/torretasUser/torreta2/nivel1/tile003.png",
			"assets/torretasUser/torreta2/nivel1/tile004.png",
			"assets/torretasUser/torreta2/nivel1/tile005.png",
			"assets/torretasUser/torreta2/nivel1/tile006.png",
			"assets/torretasUser/torreta2/nivel1/tile007.png"};
	public static final String[] TORRE_2_LVL_2_STOP = {"assets/torretasUser/torreta2/nivel2/tile000.png"};
	public static final String[] TORRE_2_LVL_2_MOV = {"assets/torretasUser/torreta2/nivel2/tile000.png",
			"assets/torretasUser/torreta2/nivel2/tile001.png",
			"assets/torretasUser/torreta2/nivel2/tile002.png",
			"assets/torretasUser/torreta2/nivel2/tile003.png",
			"assets/torretasUser/torreta2/nivel2/tile004.png",
			"assets/torretasUser/torreta2/nivel2/tile005.png",
			"assets/torretasUser/torreta2/nivel2/tile006.png",
			"assets/torretasUser/torreta2/nivel2/tile007.png"};
	public static final String[] TORRE_2_LVL_3_STOP = {"assets/torretasUser/torreta2/nivel3/tile000.png"};
	public static final String[] TORRE_2_LVL_3_MOV = {"assets/torretasUser/torreta2/nivel3/tile000.png",
			"assets/torretasUser/torreta2/nivel3/tile001.png",
			"assets/torretasUser/torreta2/nivel3/tile002.png",
			"assets/torretasUser/torreta2/nivel3/tile003.png",
			"assets/torretasUser/torreta2/nivel3/tile004.png",
			"assets/torretasUser/torreta2/nivel3/tile005.png",
			"assets/torretasUser/torreta2/nivel3/tile006.png",
			"assets/torretasUser/torreta2/nivel3/tile007.png"};
	public static final String[] TORRE_3_LVL_1_STOP= { "assets/torretasUser/torreta3/nivel1/tile008.png" };
	public static final String[] TORRE_3_LVL_1_MOV = { "assets/torretasUser/torreta3/nivel1/tile008.png" };
	public static final String[] TORRE_3_LVL_2_STOP= { "assets/torretasUser/torreta3/nivel2/tile009.png" };
	public static final String[] TORRE_3_LVL_2_MOV = { "assets/torretasUser/torreta3/nivel2/tile009.png" };
	public static final String[] TORRE_3_LVL_3_STOP= { "assets/torretasUser/torreta3/nivel3/tile011.png" };
	public static final String[] TORRE_3_LVL_3_MOV = { "assets/torretasUser/torreta3/nivel3/tile011.png" };

}