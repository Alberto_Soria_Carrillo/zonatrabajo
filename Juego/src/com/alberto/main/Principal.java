package com.alberto.main;

import com.albert.escenario.VentanaJuego;
import com.alberto.constantes.Const;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

/**
 * Clase Principal del programa, encargara de su arranque
 * @author cloud
 *
 */
public class Principal {

	/**
	 * Meto main del programa
	 * @param args
	 */
	public static void main(String[] args) {

		Game ventana = new VentanaJuego();
		LwjglApplication lanzador = new LwjglApplication(ventana, "Tanques", Const.anchura, Const.altura);
		

	}

}
