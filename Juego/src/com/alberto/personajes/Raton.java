package com.alberto.personajes;

import com.albert.escenario.AccionesVentana;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase Raton, se encarga de cargar la mirilla que se usa para realizar acciones en el mapa
 * @author cloud
 *
 */
public class Raton extends BaseActor {

	public Animation animation;

	// Constructor
	public Raton(float x, float y, Stage s, AccionesVentana accionesVentana) {
		super(x, y, s);

		String[] imagen = { "assets/decoracion/Untitled_drawing_5.png" };

		animation = loadAnimationFromFiles(imagen, 1f, true);

	}

	// Metodo desactiva. uso pruebas
	public void act() {

		// realiza la escucha de los evento de posicion
		if (Gdx.input.isTouched()) {

			this.setX(Gdx.input.getX());
			this.setY(Gdx.input.getY());
		}
		
		BaseActor.setWorldBounds(800, 600);
		this.alignCamera();
		this.boundToWorld();
	}
	
	

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		super.draw(batch, parentAlpha);
	}

}
