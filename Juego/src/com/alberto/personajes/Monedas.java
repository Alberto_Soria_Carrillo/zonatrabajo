package com.alberto.personajes;

import com.albert.escenario.AccionesVentana;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase Monedas encarga de cargar las monedas de recompensa por subir de nivel
 * @author cloud
 *
 */
public class Monedas extends BaseActor {
	// Atributos
	public AccionesVentana accionesVentana;
	public boolean vivo;
	public Animation animation;
	
	// Contructor
	public Monedas(float x, float y, Stage s, AccionesVentana accionesVentana) {
		super(x, y, s);

		this.accionesVentana = accionesVentana;

		this.vivo = true;
		
		String[] imagen = {"assets\\decoracion\\monedas\\tile000.png","assets\\decoracion\\monedas\\tile001.png",
				"assets\\decoracion\\monedas\\tile002.png","assets\\decoracion\\monedas\\tile003.png",
				"assets\\decoracion\\monedas\\tile004.png","assets\\decoracion\\monedas\\tile005.png",
				"assets\\decoracion\\monedas\\tile006.png","assets\\decoracion\\monedas\\tile007.png",
				"assets\\decoracion\\monedas\\tile008.png","assets\\decoracion\\monedas\\tile009.png",
				"assets\\decoracion\\monedas\\tile010.png","assets\\decoracion\\monedas\\tile011.png",
				"assets\\decoracion\\monedas\\tile012.png","assets\\decoracion\\monedas\\tile013.png",
				"assets\\decoracion\\monedas\\tile014.png","assets\\decoracion\\monedas\\tile015.png"};
		animation = loadAnimationFromFiles(imagen, 0.07f, true);
		this.setAnimation(animation);

	}

	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub
		if (vivo) {
			super.act(dt);
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (vivo)
			super.draw(batch, parentAlpha);
	}

}
