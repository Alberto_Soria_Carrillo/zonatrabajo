package com.alberto.personajes;

import com.albert.escenario.AccionesVentana;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.audio.Mp3.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase Tanke encarga de las acciones de los tankes y de cargar la clase explosion
 * 
 * @author cloud
 *
 */
public class Tanke extends BaseActor {

	// Atrinutos
	public AccionesVentana accionesVentena;

	public Animation andando;
	public Animation parado;
	public TorreTanke torre;
	public Stage s;
	public Explosion explosion;
	public boolean vivo;

	// Constructor
	public Tanke(float x, float y, Stage s, AccionesVentana accionesVentana, int tipo, int camino) {
		super(x, y, s);

		this.s = s;
		this.accionesVentena = accionesVentana;
		this.explosion = new Explosion(this.getX(), this.getY(), s);
		this.vivo = true;
		this.setBoundaryPolygon(8);

		parado = loadTexture("assets/tanques/tile000.png");

		String[] walkFileNames = { "assets/tanques/tile000.png", "assets/tanques/tile001.png" };
		andando = loadAnimationFromFiles(walkFileNames, 0.2f, true);// imagenes, duración de frame, si es animación en
																	// bucle
		torre = new TorreTanke(x, y, s, accionesVentana, tipo, camino);

	}

	@Override
	public void act(float dt) {

		/*
		 * Sincroniza el movimiento de la torreta y el tanke. Se realiza en esta clase
		 * para que exista un poco de dessincronia que la cual hace que el persona
		 * parezca menos rigido
		 */
		this.setX(torre.getX());
		this.setY(torre.getY());

		// pone la animacion segun el movimiento
		if (this.velocityVec.x != 0 || this.velocityVec.y != 0) {

			this.setAnimation(andando);

		}

		// sincorniza la activacion de ambos elementos
		this.vivo = torre.vivo;

		// En caso de morir el tanqke activa la exploxion en el lugar donde se ubique
		if (this.torre.vida <= 0) {
			explosion.setX(this.getX());
			explosion.setY(this.getY());
			explosion.vivo = true;

		}

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (vivo) {
			super.draw(batch, parentAlpha);
		}
	}

}
