package com.alberto.personajes;

import com.albert.escenario.AccionesVentana;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase Champi, se encarga de reproducir en pantalla el champi�on y de
 * controlar la ejecucion de su bufo
 * 
 * @author cloud
 *
 */
public class Champi extends BaseActor {

	public AccionesVentana accionesVentana;
	public boolean vivo;
	public Animation animation;
	public float aceleracion;
	public float deceleracion;
	public int count;

	public Champi(float x, float y, Stage s, AccionesVentana accionesVentana) {
		super(x, y, s);

		this.accionesVentana = accionesVentana;
		this.aceleracion = 64;
		this.deceleracion = 32;
		this.count = 0;
		this.vivo = false;

		// controla que tipo de imagenes se debe cargar segun el mapa
		if (accionesVentana.lvl % 2 == 0) {
			String[] imagen = { "assets\\decoracion\\champi\\tile010.png", "assets\\decoracion\\champi\\tile011.png" };
			animation = loadAnimationFromFiles(imagen, 0.2f, true);
			this.setAnimation(animation);
		} else {
			String[] imagen = { "assets\\decoracion\\champi\\tile013.png", "assets\\decoracion\\champi\\tile014.png" };
			animation = loadAnimationFromFiles(imagen, 0.2f, true);
			this.setAnimation(animation);
		}
	}

	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub
		if (vivo) {
			super.act(dt);

			movi(dt);

		}

	}

	/**
	 * Controla el movimiento del champi�on por la pantalla
	 * 
	 * @param dt
	 */
	private void movi(float dt) {
		parada(dt);

		// controla si llega  o no al punto x y de la casilla
		if (((int) this.getX() != (int) this.accionesVentana.caminoChampi.get(count).getX()
				|| (int) this.getY() != (int) this.accionesVentana.caminoChampi.get(count).getY()) && vivo) {

			// dependindo del desfase respecto al eje xy de la casilla se mueve de manera horizontal o vertical
			if ((int) this.getX() != (int) this.accionesVentana.caminoChampi.get(count).getX()) {

				if ((int) this.getX() > (int) this.accionesVentana.caminoChampi.get(count).getX()) {
					accelerationVec.add(-aceleracion * 2, 0);
				} else {
					accelerationVec.add(aceleracion * 2, 0);
				}

			}

			if (((int) this.getY()) != ((int) this.accionesVentana.caminoChampi.get(count).getY())) {

				if ((int) this.getY() > (int) this.accionesVentana.caminoChampi.get(count).getY()) {
					accelerationVec.add(0, -aceleracion);
				} else {
					accelerationVec.add(0, aceleracion);
				}
			}

		} else if (vivo) {
			// en caso de llegar al punto x y de la casilla suma uno al a la posicin del array
			count += 1;
			if (count > this.accionesVentana.caminoChampi.size()) {
				// si llegal al final del array vuelve a empezar
				count = 0;
			}
		}

		this.velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);

		this.moveBy(velocityVec.x * dt, velocityVec.y * dt);
		accelerationVec.set(0, 0);

	}

	private void parada(float dt) {
		float cantidadDecelerandoX = deceleracion * dt;
		float cantidadDecelerandoY = (deceleracion * dt) / 2;

		float direccionAndandoX;
		float direccionAndandoY;

		if (velocityVec.x > 0) {
			direccionAndandoX = 1;
		} else {
			direccionAndandoX = -1;
		}

		float velocidadAndandoX = Math.abs(velocityVec.x);
		velocidadAndandoX -= cantidadDecelerandoX;

		if (velocidadAndandoX < 0) {
			velocidadAndandoX = 0;
		}

		if (velocityVec.y > 0) {
			direccionAndandoY = 1;
		} else {
			direccionAndandoY = -1;
		}

		float velocidadAndandoY = Math.abs(velocityVec.y);
		velocidadAndandoY -= cantidadDecelerandoY;

		if (velocidadAndandoY < 0) {
			velocidadAndandoY = 0;
		}

		velocityVec.x = velocidadAndandoX * direccionAndandoX;
		velocityVec.y = velocidadAndandoY * direccionAndandoY;

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (vivo) {
			super.draw(batch, parentAlpha);
		}
	}

}
