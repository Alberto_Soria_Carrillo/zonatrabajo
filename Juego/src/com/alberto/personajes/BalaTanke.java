package com.alberto.personajes;

import com.albert.escenario.AccionesVentana;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;
import Base.Enemigo;

/**
 * Clase BalaTanke, se encarga de la animacion y acciones de las balas de los tankes
 * @author cloud
 *
 */
public class BalaTanke extends BaseActor {
	// Atributos
	public AccionesVentana accionesVentana;
	public TorreTanke torreTanke;
	public TorretaUser torretaUser;
	public Animation animation;
	public Stage s;

	// Atributos para gestionar la vida del proyectil
	private float tiempoBala; // tiempo que el proyectil permanecer� en pantalla una vez disparado
	private float velocidadBala; // velocidad a la que se mueve el proyectil
	private float cuentaBala;// variable para llevar la cuenta del tiempo que la bala lleva en pantalla
	private int direccion;// 1= derecha -1=izquierda
	private boolean enabled;

	protected float cuentaTiempo;

	public int aceleracion;
	public int deceleracion;

	// Contructor
	public BalaTanke(float x, float y, Stage s, TorreTanke torreTanke, AccionesVentana accionesVentana,
			String[] bala2) {
		super(x, y, s);

		this.s = s;
		this.torreTanke = torreTanke;
		this.accionesVentana = accionesVentana;
		enabled = false;
		animation = loadAnimationFromFiles(bala2, 0.2f, true);
		this.setAnimation(animation);
		tiempoBala = 10;
		velocidadBala = 50;
		this.aceleracion = 100;
		this.deceleracion = 100;

		this.setX(this.torreTanke.getX());
		this.setY(this.torreTanke.getY());
	}

	@Override
	public void act(float dt) {
		super.act(dt);

		// controla si la bala a sido disparada
		if (enabled) {
			// carga la animacion
			this.setAnimation(animation);

			moveBy(velocityVec.x * dt, velocityVec.y * dt);// actualizar posici�n del proyectil seg�n su velocidad
			// cuentaBala -= dt; // actualizamos el tiempo que le queda en pantalla
			if (((int) this.torretaUser.getX() != (int) this.getX())
					|| ((int) this.torretaUser.getY() != (int) this.getY())) {

				movimientoAutomatico(dt);

			} else {

				if (this.torretaUser.vivo && this.enabled && this.overlaps(this.torretaUser)) {

					this.torretaUser.disparo(this.torreTanke.atk);// ejecutamos la funci�n de da�o del enemigo impactado
					this.enabled = false; // desactivamos la bala tras el impacto
				} else {
					this.enabled = false;
				}

			}

		}

		BaseActor.setWorldBounds(800, 608);
		this.alignCamera();
		this.boundToWorld();

	}

	/**
	 * Realiza los movimiento de manera automatica hasta alcanzar la posicion de su objetivo
	 * @param dt
	 */
	private void movimientoAutomatico(float dt) {
		parada(dt);
		if ((int) this.getX() != (int) this.torretaUser.getX()) {

			if ((int) this.getX() > (int) this.torretaUser.getX()) {
				accelerationVec.add(-aceleracion * 2, 0);

				this.setScaleY(-1);
			} else {
				accelerationVec.add(aceleracion * 2, 0);

				this.setScaleY(1);
			}

		}

		if (((int) this.getY()) != ((int) this.torretaUser.getY())) {

			if ((int) this.getY() > (int) this.torretaUser.getY()) {
				accelerationVec.add(0, -aceleracion);

			} else {
				accelerationVec.add(0, aceleracion);

			}
		}

		this.velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
		this.moveBy(velocityVec.x * dt, velocityVec.y * dt);
		accelerationVec.set(0, 0);

	}

	/**
	 * Realiza la carga de los datos necesarios para localizar la ubicacion de la torre
	 * @param torreta
	 */
	public void disparar(TorretaUser torreta) {

		this.enabled = true;

		this.torretaUser = torreta;
		this.setX(this.torreTanke.getX());
		this.setY(this.torreTanke.getY());

	}

	/**
	 * Realiza las acciones de freno y parada
	 * @param dt
	 */
	private void parada(float dt) {
		float cantidadDecelerandoX = deceleracion * dt;
		float cantidadDecelerandoY = (deceleracion * dt) / 2;

		float direccionAndandoX;
		float direccionAndandoY;

		if (velocityVec.x > 0) {
			direccionAndandoX = 1;
		} else {
			direccionAndandoX = -1;
		}

		float velocidadAndandoX = Math.abs(velocityVec.x);
		velocidadAndandoX -= cantidadDecelerandoX;

		if (velocidadAndandoX < 0) {
			velocidadAndandoX = 0;
		}

		if (velocityVec.y > 0) {
			direccionAndandoY = 1;
		} else {
			direccionAndandoY = -1;
		}

		float velocidadAndandoY = Math.abs(velocityVec.y);
		velocidadAndandoY -= cantidadDecelerandoY;

		if (velocidadAndandoY < 0) {
			velocidadAndandoY = 0;
		}

		velocityVec.x = velocidadAndandoX * direccionAndandoX;
		velocityVec.y = velocidadAndandoY * direccionAndandoY;

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub

		if (enabled) {
			super.draw(batch, parentAlpha);
		}

	}

}
