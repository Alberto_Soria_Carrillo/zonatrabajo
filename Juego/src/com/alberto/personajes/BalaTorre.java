package com.alberto.personajes;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase BalaTorre, se encarga de la accion de la bala
 * @author cloud
 *
 */
public class BalaTorre extends BaseActor {

	// Atributos
	public TorretaUser torreUser;
	public TorreTanke torre;
	public Stage stage;
	public Animation animation;
	public boolean vivo;
	public float aceleracion;
	public float deceleracion;

	// Constructor
	public BalaTorre(float x, float y, Stage s, TorretaUser torreUser) {
		super(x, y, s);

		this.torreUser = torreUser;
		String[] bala = { "assets\\balas\\red.png" };
		this.animation = loadAnimationFromFiles(bala, 0.2f, true);
		this.stage = s;
		this.vivo = false;
		this.aceleracion = 64;
		this.deceleracion = 64;

	}

	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub
		super.act(dt);
		// controla si la ba�a a sido disparada
		if (vivo) {
			// Carga la animacion
			this.setAnimation(animation);
			moveBy(velocityVec.x * dt, velocityVec.y * dt);// actualizar posici�n del proyectil seg�n su velocidad
			// cuentaBala -= dt; // actualizamos el tiempo que le queda en pantalla
			if (((int) this.torre.getX() != (int) this.getX()) || ((int) this.torre.getY() != (int) this.getY())) {

				movimientoAutomatico(dt);

			} else {
				// si llega a su objetivo realiza la accion
				if (this.torre.vivo && this.vivo && this.overlaps(this.torre)) {

					this.torre.disparo(this.torreUser.dano);
					this.vivo = false;
				} else {
					// si al llegar su objetivo no existe o ya a sido destruido desaparece
					this.vivo = false;
				}

			}

		}

		BaseActor.setWorldBounds(800, 608);
		this.alignCamera();
		this.boundToWorld();

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (vivo) {
			super.draw(batch, parentAlpha);
		}
	}

	public void disparo(TorreTanke torre) {

		this.torre = torre;
		this.torreUser.disparar();
		vivo = true;
	}

	private void movimientoAutomatico(float dt) {
		parada(dt);
		if ((int) this.getX() != (int) this.torre.getX()) {

			if ((int) this.getX() > (int) this.torre.getX()) {
				accelerationVec.add(-aceleracion * 2, 0);

				this.setScaleY(-1);
			} else {
				accelerationVec.add(aceleracion * 2, 0);

				this.setScaleY(1);
			}

		}

		if (((int) this.getY()) != ((int) this.torre.getY())) {

			if ((int) this.getY() > (int) this.torre.getY()) {
				accelerationVec.add(0, -aceleracion);

			} else {
				accelerationVec.add(0, aceleracion);

			}
		}

		this.velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
		this.moveBy(velocityVec.x * dt, velocityVec.y * dt);
		accelerationVec.set(0, 0);

	}

	private void parada(float dt) {
		float cantidadDecelerandoX = deceleracion * dt;
		float cantidadDecelerandoY = (deceleracion * dt) / 2;

		float direccionAndandoX;
		float direccionAndandoY;

		if (velocityVec.x > 0) {
			direccionAndandoX = 1;
		} else {
			direccionAndandoX = -1;
		}

		float velocidadAndandoX = Math.abs(velocityVec.x);
		velocidadAndandoX -= cantidadDecelerandoX;

		if (velocidadAndandoX < 0) {
			velocidadAndandoX = 0;
		}

		if (velocityVec.y > 0) {
			direccionAndandoY = 1;
		} else {
			direccionAndandoY = -1;
		}

		float velocidadAndandoY = Math.abs(velocityVec.y);
		velocidadAndandoY -= cantidadDecelerandoY;

		if (velocidadAndandoY < 0) {
			velocidadAndandoY = 0;
		}

		velocityVec.x = velocidadAndandoX * direccionAndandoX;
		velocityVec.y = velocidadAndandoY * direccionAndandoY;

	}

}
