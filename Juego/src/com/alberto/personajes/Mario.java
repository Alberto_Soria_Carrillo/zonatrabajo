package com.alberto.personajes;

import com.albert.escenario.AccionesVentana;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase Mario, se encarga de reproducir las imagenes de mario, y del momento de ejecucion del bufo
 * @author cloud
 *
 */
public class Mario extends BaseActor {
	// atributos
	public AccionesVentana accionesVentana;
	public boolean vivo;
	public Animation animation;
	// Constructor
	public Mario(float x, float y, Stage s, AccionesVentana accionesVentana) {
		super(x, y, s);

		this.accionesVentana = accionesVentana;

		this.vivo = false;

		String[] imagen = { "assets\\decoracion\\mario\\tile000.png", "assets\\decoracion\\mario\\tile001.png",
				"assets\\decoracion\\mario\\tile002.png", "assets\\decoracion\\mario\\tile003.png",
				"assets\\decoracion\\mario\\tile004.png", "assets\\decoracion\\mario\\tile003.png",
				"assets\\decoracion\\mario\\tile002.png", "assets\\decoracion\\mario\\tile001.png",
				"assets\\decoracion\\mario\\tile000.png" };
		animation = loadAnimationFromFiles(imagen, 0.2f, true);
		this.setAnimation(animation);

	}

	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub
		if (vivo) {
			super.act(dt);
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (vivo) {
			super.draw(batch, parentAlpha);
		}
	}
}
