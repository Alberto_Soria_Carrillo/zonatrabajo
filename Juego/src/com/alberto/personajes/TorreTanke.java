package com.alberto.personajes;

import java.util.ArrayList;

import com.albert.escenario.AccionesVentana;
import com.albert.escenario.Camino;
import com.alberto.constantes.Const;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BalaMosca;
import Base.BaseActor;
import com.alberto.personajes.BalaTanke;

/**
 * Clase TorreTanke, se encarga de las acciones de la Torreta de los tankes
 * @author cloud
 *
 */
public class TorreTanke extends BaseActor {

	// Atributos
	public AccionesVentana accionesVentana;
	public float vida;
	public boolean vivo;
	public float atk;
	public String defensa;
	public float aceleracion;
	public float deceleracion;
	public Animation disparo;
	public int coint;
	public Stage s;
	public float velocidad;
	public float porcentajeDisparos;
	public int count;
	public int camino;
	public BalaTanke bala;
	public ArrayList<BalaTanke> balas; // balas de las que dispone el tanke
	private float cuentaCoolDownBala;// variable para llevar la cuenta del cooldown anterior
	private float coolDownBala;// tiempo que tiene que transcurrir desde un disparo hasta que se pueda volver a
								// dispara
	private int balaActual; // numero de bala actual que usa
	private int numBalas; // numero de balas que le quedan

	// Constructor
	public TorreTanke(float x, float y, Stage s, AccionesVentana accionesVentana, int tipo, int camino) {
		super(x, y, s);

		this.accionesVentana = accionesVentana;
		this.s = s;

		balas = new ArrayList<BalaTanke>();

		seleccionTipo(tipo);
		setAnimation(disparo);
		atk = atk * this.accionesVentana.lvl;
		vida = vida * this.accionesVentana.lvl;

		this.camino = camino;
		this.coolDownBala = 10;
		this.cuentaCoolDownBala = 10 / this.accionesVentana.lvl;
		this.balaActual = 0;
		this.numBalas = 0;
		this.count = 0;
		this.vivo = false;

	}

	@Override
	public void act(float dt) {
		// movimientoManual(dt);

		if (vivo) {
			// controla la velocidad de diparo y si es necesario le resta el delta time
			if (cuentaCoolDownBala > 0) {
				this.cuentaCoolDownBala -= dt;
			}

			// Si el tiempo de espera para diparar a terminado realiza las siguientes acciones
			if (this.cuentaCoolDownBala <= 0 && (this.balaActual <= this.numBalas)) {
				// busca torretas defensivas a su alcance y si las encuentra les dispara
				for (TorretaUser aux : this.accionesVentana.torretaUser) {

					if ((Math.abs(aux.getX()) - Math.abs(this.getX()) < 124)
							|| (Math.abs(aux.getY()) - Math.abs(this.getY()) < 124)) {

						this.disparar(aux);

						break;
					}

				}

			} else if (vivo && this.cuentaCoolDownBala > 0) {
				movimientoAutomatico(dt);
			}

			if (this.cuentaCoolDownBala < 0) {
				this.cuentaCoolDownBala = coolDownBala;
			}

			if (vida <= 0) {
				vivo = false;
				this.accionesVentana.countTankes -= 1;
				this.accionesVentana.tankesDead += 1;
				this.accionesVentana.tankesDeadInLvl += 1;
				this.accionesVentana.coint += this.coint;
			}
		}
		BaseActor.setWorldBounds(800, 608);
		this.alignCamera();
		this.boundToWorld();

	}

	/*
	 * Controla las balas al diparar a una torreta defensiva
	 */
	private void disparar(TorretaUser aux) {
		// activa la bala
		balas.get(balaActual).disparar(aux);
		// modifica el contador de valas
		balaActual++;

	}

	/*
	 * Controla que movimiento debe realizar dependiendo del mapa que se encuentre
	 */
	private void movimientoAutomatico(float dt) {

		if (camino == 1) {
			camino1(this.accionesVentana.camino1, dt);

		} else {
			camino1(this.accionesVentana.camino2, dt);

		}

	}

	/*
	 * Se encarga de mover el tanque por el camino, localizando la cenda sigiente del array y moviemdose hasta posicionarse en el punto x y de la misma
	 */
	private void camino1(ArrayList<Camino> camino1, float dt) {

		// Reduce la velocidad para falicilar que termine coincidiendo con la celda
		parada(dt);

		// Comprueba que no esta en la celda indicada
		if (((int) this.getX() != (int) camino1.get(count).getX()
				|| (int) this.getY() != (int) camino1.get(count).getY()) && vivo) {

			// se mueve de manera horizontal o vertical dependiendo de si esta desplazado sobre el eje x o y respecto a la celda
			
			if ((int) this.getX() != (int) camino1.get(count).getX()) {

				if ((int) this.getX() > (int) camino1.get(count).getX()) {
					accelerationVec.add(-aceleracion * 2, 0);
					this.setRotation(90);
					this.setScaleY(-1);
				} else {
					accelerationVec.add(aceleracion * 2, 0);
					this.setRotation(270);
					this.setScaleY(1);
				}

			}

			if (((int) this.getY()) != ((int) camino1.get(count).getY())) {

				if ((int) this.getY() > (int) camino1.get(count).getY()) {
					accelerationVec.add(0, -aceleracion);
					this.setRotation(180);
				} else {
					accelerationVec.add(0, aceleracion);
					this.setRotation(360);
				}
			}
			
		} else if (vivo) {
			// en caso de coincidir con la casilla suma 1 al array para saber el puno x e y de la siguiente casilla
			count += 1;
			// en caso de llegar al final realiza las siguientes acciones
			if (count >= camino1.size() - 1) {
				// resta vida al jugador
				this.accionesVentana.vida = (int) (this.accionesVentana.vida - this.vida);
				// Controla la cantidad de tankes en pantalla
				this.accionesVentana.countTankes -= 1;
				// desactiva esta torreta
				vivo = false;
			}
		}

		this.velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
		this.moveBy(velocityVec.x * dt, velocityVec.y * dt);
		accelerationVec.set(0, 0);

	}

	/**
	 * Carga los atributos de las torretas de los tankes
	 * 
	 * @param tipo
	 */
	private void seleccionTipo(int tipo) {
		switch (tipo) {
		case 1:

			atk = 10;
			vida = 120;
			aceleracion = 350;
			deceleracion = 450;
			velocidad = 500;
			porcentajeDisparos = (float) 0.8;
			coint = Const.PRICE_T_1;
			defensa = "";

			String[] walkFileNames1 = { "assets/torretasUser/torreta1/nivel1/tile000.png",
					"assets/torretasUser/torreta1/nivel1/tile001.png",
					"assets/torretasUser/torreta1/nivel1/tile002.png",
					"assets/torretasUser/torreta1/nivel1/tile003.png",
					"assets/torretasUser/torreta1/nivel1/tile004.png",
					"assets/torretasUser/torreta1/nivel1/tile005.png",
					"assets/torretasUser/torreta1/nivel1/tile006.png",
					"assets/torretasUser/torreta1/nivel1/tile007.png" };
			disparo = loadAnimationFromFiles(walkFileNames1, 0.2f, true);// imagenes, duración de frame, si es animación
																			// en bucle
			String[] bala1 = { "assets\\balas\\white.png" };
			cargarBalas(bala1);
			break;
		case 2:

			atk = 20;
			vida = 140;
			aceleracion = 250;
			deceleracion = 300;
			velocidad = 300;
			porcentajeDisparos = (float) 0.5;
			coint = Const.PRICE_T_2;
			defensa = "";

			String[] walkFileNames2 = { "assets/torretasUser/torreta2/nivel1/tile000.png",
					"assets/torretasUser/torreta2/nivel1/tile001.png",
					"assets/torretasUser/torreta2/nivel1/tile002.png",
					"assets/torretasUser/torreta2/nivel1/tile003.png",
					"assets/torretasUser/torreta2/nivel1/tile004.png",
					"assets/torretasUser/torreta2/nivel1/tile005.png",
					"assets/torretasUser/torreta2/nivel1/tile006.png",
					"assets/torretasUser/torreta2/nivel1/tile007.png" };
			disparo = loadAnimationFromFiles(walkFileNames2, 0.2f, true);// imagenes, duración de frame, si es animación
																			// en bucle
			String[] bala2 = { "assets\\balas\\white.png" };
			cargarBalas(bala2);
			break;
		case 3:

			atk = 30;
			vida = 160;
			aceleracion = 78;
			deceleracion = 128;
			velocidad = 250;
			porcentajeDisparos = (float) 0.3;
			coint = Const.PRICE_T_3;
			defensa = "";

			String[] walkFileNames3 = { "assets/torretasUser/torreta3/nivel1/tile008.png" };
			disparo = loadAnimationFromFiles(walkFileNames3, 0.2f, true);// imagenes, duración de frame, si es animación
																			// en bucle
			String[] bala3 = { "assets\\balas\\white.png" }; //
			cargarBalas(bala3);
			break;
		}

	}

	/**
	 * Carga las balas al cargar las torretas de los tankes
	 * 
	 * @param bala2
	 */
	private void cargarBalas(String[] bala2) {

		for (int i = 0; i < 30; i++) {

			bala = new BalaTanke(this.getX() - 16, this.getY() + 16, this.s, this, this.accionesVentana, bala2);
			balas.add(bala);
		}

		this.numBalas = balas.size() - 1;

	}

	// Permite mover de manera manual los tankes
	private void movimientoManual(float dt) {

		if (Gdx.input.isKeyPressed(Keys.D)) {
			accelerationVec.add(aceleracion, 0);

		}

		if (Gdx.input.isKeyPressed(Keys.A)) {
			accelerationVec.add(-aceleracion, 0);

		}

		if (Gdx.input.isKeyPressed(Keys.W)) {
			accelerationVec.add(0, aceleracion);

		}

		if (Gdx.input.isKeyPressed(Keys.S)) {
			accelerationVec.add(0, -aceleracion);

		}

		if (!Gdx.input.isKeyPressed(Keys.A) && !Gdx.input.isKeyPressed(Keys.D) && !Gdx.input.isKeyPressed(Keys.LEFT)
				&& !Gdx.input.isKeyPressed(Keys.RIGHT)) {// Si no hay ninguna tecla de movimiento pulsada hay que
															// decelerar
			parada(dt);

		}

		this.velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
		this.moveBy(velocityVec.x * dt, velocityVec.y * dt);
		accelerationVec.set(0, 0);

	}

	/**
	 * Reduce y para la velocidad de movimiento
	 * 
	 * @param dt
	 */
	private void parada(float dt) {
		float cantidadDecelerandoX = deceleracion * dt;
		float cantidadDecelerandoY = (deceleracion * dt) / 2;

		float direccionAndandoX;
		float direccionAndandoY;

		if (velocityVec.x > 0) {
			direccionAndandoX = 1;
		} else {
			direccionAndandoX = -1;
		}

		float velocidadAndandoX = Math.abs(velocityVec.x);
		velocidadAndandoX -= cantidadDecelerandoX;

		if (velocidadAndandoX < 0) {
			velocidadAndandoX = 0;
		}

		if (velocityVec.y > 0) {
			direccionAndandoY = 1;
		} else {
			direccionAndandoY = -1;
		}

		float velocidadAndandoY = Math.abs(velocityVec.y);
		velocidadAndandoY -= cantidadDecelerandoY;

		if (velocidadAndandoY < 0) {
			velocidadAndandoY = 0;
		}

		velocityVec.x = velocidadAndandoX * direccionAndandoX;
		velocityVec.y = velocidadAndandoY * direccionAndandoY;

	}

	/**
	 * Se encarga de restar vida al ser alcanzado
	 * 
	 * @param atk2
	 */
	public void disparo(float atk2) {

		this.vida -= atk2;

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (vivo) {
			super.draw(batch, parentAlpha);
		}
	}

}
