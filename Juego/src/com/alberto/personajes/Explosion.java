package com.alberto.personajes;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase Explosion, se encarga de realizar la explosion al destruirse los elementos del juego
 * @author cloud
 *
 */
public class Explosion extends BaseActor {
	// Atributos
	public boolean vivo;
	public Animation animation;
	public float time;
	// Contructor
	public Explosion(float x, float y, Stage s) {
		super(x, y, s);

		this.time = 1.4f;
		this.vivo = false;
		String[] explosion = { "assets\\decoracion\\explosion\\Explosion_1.png",
				"assets\\decoracion\\explosion\\Explosion_2.png", "assets\\decoracion\\explosion\\Explosion_3.png",
				"assets\\decoracion\\explosion\\Explosion_4.png", "assets\\decoracion\\explosion\\Explosion_5.png",
				"assets\\decoracion\\explosion\\Explosion_6.png", "assets\\decoracion\\explosion\\Explosion_7.png", };
		this.animation = loadAnimationFromFiles(explosion, 0.2f, true);

		

	}

	@Override
	public void act(float dt) {
		// controla el inicio de la animacion
		if (vivo && time >= 0) {
			super.act(dt);

			this.setAnimation(animation);

			time -= dt;
		}
		
		// controla el fin de la animacion
		if (time < 0) {
			vivo = false;
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (vivo)
			super.draw(batch, parentAlpha);
	}

}
