package com.alberto.personajes;

import java.util.ArrayList;

import com.albert.escenario.AccionesVentana;
import com.albert.escenario.PositionTorreUser;
import com.alberto.constantes.Const;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase TorretaUser, encarga de la invocacion de la torretas defensivas
 * @author cloud
 *
 */
public class TorretaUser extends BaseActor {

	// Atributos
	private AccionesVentana accionesVenata;
	public Stage s;
	public PositionTorreUser pt;
	public BalaTorre bala;
	public ArrayList<BalaTorre> balas;
	public Animation imagen1;
	public Animation imagen2;
	public Animation imagen3;
	public Animation imagenMov1;
	public Animation imagenMov3;
	public Animation imagenMov2;
	public Explosion explosion;

	public int lvl;
	public int controLvl;
	public int vida;
	public boolean vivo;
	public int dano;
	public Sound disparo;
	public float tiempoDisparo;
	public float tiempoEsperaDisparo;
	public float volumen;
	public int tipo;
	public int countBalas;
	public int controlBalas;
	
	// Constructor
	public TorretaUser(float x, float y, Stage s, AccionesVentana accionesVentana, PositionTorreUser pt, int vida,
			int dano, float velocidadDisparo, int tipo, float volumen, Sound frase, Sound disparo) {
		super(x, y, s);
		
		this.balas = new ArrayList<>();

		this.s = s;
		this.lvl = 1;
		this.controLvl = lvl;
		this.tipo = tipo;
		this.accionesVenata = accionesVentana;
		this.vivo = true;
		this.vida = vida;
		this.dano = dano;
		this.disparo = disparo;
		this.pt = pt;
		this.volumen = volumen;
		this.tiempoDisparo = velocidadDisparo;
		this.tiempoEsperaDisparo = 3;
		this.controlBalas = 0;

		cargarImagenes();

		frase.play(this.volumen);
		this.explosion = new Explosion(this.getX(),this.getY(),s);
		controlLvl();
		cargarBala();
	}

	/**
	 * Carga las imagenes al cargar la torre
	 */
	private void cargarImagenes() {
		switch (tipo) {
		case 1:

			this.imagen1 = loadAnimationFromFiles(Const.TORRE_1_LVL_1_STOP, 0.5f, true);
			this.imagenMov1 = loadAnimationFromFiles(Const.TORRE_1_LVL_1_MOV, 0.2f, true);

			this.imagen2 = loadAnimationFromFiles(Const.TORRE_1_LVL_2_STOP, 0.5f, true);
			this.imagenMov2 = loadAnimationFromFiles(Const.TORRE_1_LVL_2_MOV, 0.2f, true);

			this.imagen3 = loadAnimationFromFiles(Const.TORRE_1_LVL_3_STOP, 0.5f, true);
			this.imagenMov3 = loadAnimationFromFiles(Const.TORRE_1_LVL_3_MOV, 0.2f, true);

			break;
		case 2:

			this.imagen1 = loadAnimationFromFiles(Const.TORRE_2_LVL_1_STOP, 0.5f, true);
			this.imagenMov1 = loadAnimationFromFiles(Const.TORRE_2_LVL_1_MOV, 0.2f, true);

			this.imagen2 = loadAnimationFromFiles(Const.TORRE_2_LVL_2_STOP, 0.5f, true);
			this.imagenMov2 = loadAnimationFromFiles(Const.TORRE_2_LVL_2_MOV, 0.2f, true);

			this.imagen3 = loadAnimationFromFiles(Const.TORRE_2_LVL_3_STOP, 0.5f, true);
			this.imagenMov3 = loadAnimationFromFiles(Const.TORRE_2_LVL_3_MOV, 0.2f, true);

			break;
		case 3:

			this.imagen1 = loadAnimationFromFiles(Const.TORRE_3_LVL_1_STOP, 0.5f, true);
			this.imagenMov1 = loadAnimationFromFiles(Const.TORRE_3_LVL_1_MOV, 0.2f, true);

			this.imagen2 = loadAnimationFromFiles(Const.TORRE_3_LVL_2_STOP, 0.5f, true);
			this.imagenMov2 = loadAnimationFromFiles(Const.TORRE_3_LVL_2_MOV, 0.2f, true);

			this.imagen3 = loadAnimationFromFiles(Const.TORRE_3_LVL_3_STOP, 0.5f, true);
			this.imagenMov3 = loadAnimationFromFiles(Const.TORRE_3_LVL_3_MOV, 0.2f, true);

			break;
		}

	}
	
	/**
	 * Carga las balas al cargar la Torre
	 */
	private void cargarBala() {

		for (int i = 0; i < 100; i++) {
			bala = new BalaTorre(this.getX(), this.getY(), this.s, this);
			balas.add(bala);
		}

		this.countBalas = balas.size();

	}

	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub
		super.act(dt);
		// Controla que la accion solo se realiza si esta viva
		if (this.vida <= 0) {
			// acciones que se realizan en caso de ser destruida
			// cambia el boolean para indicar que esta desactivada
			vivo = false;
			// Libera el espacio para poder posicionar otra torre
			pt.enUso = false;
			// Para la reproduccion del sonido
			this.disparo.stop();
			// Posiciona la imagen de la explosion en el lugar donde se ecneuntra la torre
			explosion.setX(this.getX());
			explosion.setY(this.getY());
			// Activa la secuencia de la explosion
			explosion.vivo = true;
		} else {
			
			// controla el ritmo de los disparos
			if (this.tiempoEsperaDisparo <= 0) {
				
				// busca tankes dentro de su area
				for (Tanke tanke : this.accionesVenata.tankes) {

					// dispara si encuentra alguno dentro de su zona
					if (tanke.torre.vivo && ((Math.abs(this.getX() - tanke.torre.getX()) <= 98)
							&& (Math.abs(this.getY() - tanke.torre.getY()) <= 98))) {

						balas.get(controlBalas).disparo(tanke.torre);
						// controla la posicion en el array de las balas
						this.controlBalas++;
						break;
					}

				}

			}
			 // reinicia el tiempo de disparo
			if (this.tiempoEsperaDisparo < 0) {
				this.tiempoEsperaDisparo = this.tiempoDisparo;
			}

			// Resta el deltaTime al tiempo  del disparo
			if (this.tiempoEsperaDisparo != 0) {
				this.tiempoEsperaDisparo -= dt;
			}

			// Controla que el array de las balas no se salga del indice
			if (this.controlBalas == balas.size() - 1) {
				this.controlBalas = 0;
			}

			// Controla si a sido activado el evento de subida de nivel
			if (this.lvl != this.controLvl) {
				controlLvl();
				this.controLvl = this.lvl;
			}
		}

	}

	/**
	 * Controla la apariencia y status de la torre al cambiar de nivel
	 */
	private void controlLvl() {
		this.vida = vida * lvl;
		this.dano = dano * lvl;

		switch (tipo) {
		case 1:

			if (lvl == 1) {
				this.setAnimation(imagenMov1);
			} else if (lvl == 2) {
				this.setAnimation(imagenMov2);
			} else if (lvl == 3) {
				this.setAnimation(imagenMov3);
			}
			break;
		case 2:

			if (lvl == 1) {
				this.setAnimation(imagenMov1);
			} else if (lvl == 2) {
				this.setAnimation(imagenMov2);
			} else if (lvl == 3) {
				this.setAnimation(imagenMov3);
			}
			break;
		case 3:
			if (lvl == 1) {
				this.setAnimation(imagenMov1);
			} else if (lvl == 2) {
				this.setAnimation(imagenMov2);
			} else if (lvl == 3) {
				this.setAnimation(imagenMov3);
			}
			break;
		}

	}
	
	/**
	 * invoca el metodo del sonido
	 */
	public void disparar() {

		sonidoDisparo();

	}
 
	/**
	 * Reproduce el sonido del disparo
	 */
	public void sonidoDisparo() {

		disparo.play(volumen / 2);

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (vivo) {
			super.draw(batch, parentAlpha);
		}
	}

	/**
	 * Resta la vida al recibir da�o
	 * 
	 * @param atk
	 */
	public void disparo(float atk) {

		this.vida -= atk;

	}

}
