package com.albert.escenario;

import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase Camino, se encarga de guardar los datos sobre las distintas casillas del camino
 * @author cloud
 *
 */
public class Camino extends BaseActor{
	// atributos
	public boolean enabled;
	public int valor;
	//Constructor
	public Camino(float x, float y, Stage s,float width, float height, int valor) {
		super(x, y, s);
		
		this.valor = (int) valor;
		
		setSize(width, height);
		setBoundaryRectangle();
		enabled = true;
		
	}
	// Getter and Setter
	public void setEnabled(boolean b) {
		enabled = b;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
	
	

}
