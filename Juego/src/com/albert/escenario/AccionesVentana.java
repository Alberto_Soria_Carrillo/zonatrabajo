package com.albert.escenario;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import com.alberto.constantes.Const;
import com.alberto.personajes.Champi;
import com.alberto.personajes.Mario;
import com.alberto.personajes.Monedas;
import com.alberto.personajes.Raton;
import com.alberto.personajes.Tanke;
import com.alberto.personajes.TorretaUser;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.backends.lwjgl.audio.Mp3.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import Base.BaseGame;
import Base.BaseScreen;
import Base.TilemapActor;

/**
 * Clase AccionesVentana, se encarga de la gestion del juego y de la interaccion
 * entre las clases, ademas de controlar la ejecucion de los distintos elementos
 * 
 * @author cloud
 *
 */
public class AccionesVentana extends BaseScreen {
	// atributo
	public Tanke enemigo;
	public Raton raton;
	public TilemapActor tma;
	public MapProperties startProps1;
	public MapProperties startProps2;
	public MapProperties premioMoneda;
	public MapProperties premioLvlUp;
	public MapProperties premioChampi;
	public SpriteBatch b;
	public Music theme;
	public Monedas moneda;
	public Mario mario;
	public Champi champi;
	public Label extra;

	public Label vidaLabel;
	public Label cointLabel;
	public Label tankesDeadLabel;

	public ArrayList<Camino> caminoChampi;
	public ArrayList<Camino> camino1;
	public ArrayList<Camino> camino2;
	public ArrayList<Tanke> tankes;
	public ArrayList<TorretaUser> torretaUser;
	public ArrayList<PositionTorreUser> torreUser;

	public int lvl;
	public int vida;
	public boolean musica;
	public float volumen;
	public int countTankes;
	public int tankesTotalesDesplegados;
	public int tankesDead;
	public int tankesDeadInLvl;
	public int coint;
	public int torreElegida;
	public int i; // variable de control para la carga del mapa
	public float tiempoDespliege;
	public float tiempoTranscurrido;
	public boolean controlMario;
	public float controlTimeMario;
	public float timeMario;
	public boolean controlDeadMario;

	public boolean controlChampi;
	public float controlTimeChampi;
	public float timeChampi;
	public boolean controlDeadChampi;
	// atributos
	public AccionesVentana(int vida, int lvl, int tankesDead, int coint, boolean music) {
	
		this.vida = vida;
		this.lvl = lvl;
		this.tankesDead = tankesDead;
		this.tankesDeadInLvl = 0;
		this.coint = coint;
		this.i = (this.lvl % 2); // controla los caminos y los demas elemnto que se deben cargar segun el mapa que oque
		this.torreElegida = 0;
		this.volumen = 0.5f;
		this.musica = music;
		this.tiempoDespliege = 2;
		this.tiempoTranscurrido = 0;
		this.tankesTotalesDesplegados = 0;
		this.controlMario = false;
		this.controlTimeMario = 5;
		this.timeMario = 0;
		this.controlDeadMario = false;

		this.controlChampi = false;
		this.controlTimeChampi = 5;
		this.timeChampi = 0;
		this.controlDeadChampi = false;

		this.caminoChampi = new ArrayList<Camino>();
		this.camino1 = new ArrayList<Camino>();
		this.camino2 = new ArrayList<Camino>();
		this.tankes = new ArrayList<Tanke>();
		this.torreUser = new ArrayList<PositionTorreUser>();
		this.torretaUser = new ArrayList<TorretaUser>();
		this.b = new SpriteBatch();

		this.theme = Gdx.audio.newMusic(Gdx.files.internal("assets\\musica\\contra-theme-nes meloboom.mp3"));
		this.theme.setLooping(true);
		this.theme.setVolume(volumen / 2);
		if (musica) {
			this.theme.play();
		}
		
		// carga el cargador de los elementos
		carga(this.i);

	}

	/**
	 * carga los Lavel con la informacion en la pantalla
	 * @param i
	 */
	private void cargarLabel(int i) {

		vidaLabel = new Label("Vida: " + this.vida, BaseGame.labelStyle);
		cointLabel = new Label("Dinero: " + this.coint, BaseGame.labelStyle);
		tankesDeadLabel = new Label("Destruidos: " + this.tankesDead, BaseGame.labelStyle);

		uiStage.addActor(tankesDeadLabel);
		tankesDeadLabel.setPosition(10, 130);
		uiStage.addActor(vidaLabel);
		vidaLabel.setPosition(10, 65);
		uiStage.addActor(cointLabel);
		cointLabel.setPosition(10, 0);

		String[] imagen = { "assets/torretasUser/todas/tile000.png" };
		Decoracion decoracion = new Decoracion(Const.altura / 2, Const.anchura / 2, mainStage, imagen);
		uiStage.addActor(decoracion);
		decoracion.setPosition(234, 50);
		Label letra1 = new Label("Q", BaseGame.labelStyle);
		uiStage.addActor(letra1);
		letra1.setPosition(237, 80);
		Label priceT1 = new Label(String.valueOf(Const.PRICE_T_1), BaseGame.labelStyle);
		uiStage.addActor(priceT1);
		priceT1.setPosition(240, 10);

		String[] imagen1 = { "assets/torretasUser/todas/tile004.png" };
		Decoracion decoracion1 = new Decoracion(Const.altura / 2, Const.anchura / 2, mainStage, imagen1);
		uiStage.addActor(decoracion1);
		decoracion1.setPosition(270, 50);
		Label letra2 = new Label("W", BaseGame.labelStyle);
		uiStage.addActor(letra2);
		letra2.setPosition(267, 80);
		Label priceT2 = new Label(String.valueOf(Const.PRICE_T_2), BaseGame.labelStyle);
		uiStage.addActor(priceT2);
		priceT2.setPosition(275, 10);

		String[] imagen2 = { "assets/torretasUser/todas/tile008.png" };
		Decoracion decoracion2 = new Decoracion(Const.altura / 2, Const.anchura / 2, mainStage, imagen2);
		uiStage.addActor(decoracion2);
		decoracion2.setPosition(300, 50);
		Label letra3 = new Label("E", BaseGame.labelStyle);
		uiStage.addActor(letra3);
		letra3.setPosition(307, 80);
		Label priceT3 = new Label(String.valueOf(Const.PRICE_T_3), BaseGame.labelStyle);
		uiStage.addActor(priceT3);
		priceT3.setPosition(307, 10);

	}

	@Override
	public void render(float dt) {
		// TODO Auto-generated method stub
		super.render(dt);

		Gdx.input.setCursorCatched(false);

		removeLabel(i);

		b.begin();

		raton.setX(Gdx.input.getX() - 24);
		raton.setY(Gdx.graphics.getHeight() - (Gdx.input.getY() + 24));

		cargarLabel(i);

		b.end();

	}
	
	/**
	 * limpia la pantalla de labels
	 * @param i
	 */
	private void removeLabel(int i) {

		this.vidaLabel.remove();
		this.cointLabel.remove();
		this.tankesDeadLabel.remove();

	}

	@Override
	public void initialize() {

	}

	/**
	 * carga los cargadores del juego
	 * @param i
	 */
	private void carga(int i) {
		cargarCamara();
		cargarMapa(i);
		cargarCaminos(i);
		cargarPuertoStart(i);
		cargarTorres(i);
		cargarEnemigos(i);
		cargarRaton();
		cargarLabel(i);
		cargarPremioPosicion();
		cargarPremiosMoneda();
		cargarPremiosLvlUp();
		cargarPremiosChampi();

	}

	/**
	 *  carga el champi�on
	 */
	private void cargarPremiosChampi() {
		champi = new Champi((float) premioChampi.get("x"), (float) premioChampi.get("y"), mainStage, this);

	}

	/**
	 *  carga el mario
	 */
	private void cargarPremiosLvlUp() {
		mario = new Mario((float) premioLvlUp.get("x"), (float) premioLvlUp.get("y"), mainStage, this);
	}

	/**
	 *  carga la moneda
	 */
	private void cargarPremiosMoneda() {
		moneda = new Monedas((float) premioMoneda.get("x"), (float) premioMoneda.get("y"), mainStage, this);
	}

	/**
	 * carga la posicion de lo premios
	 */
	private void cargarPremioPosicion() {
		MapObject premioObj = tma.getRectangleList("premio").get(0);
		this.premioMoneda = premioObj.getProperties();

		MapObject marioObj = tma.getRectangleList("mario").get(0);
		this.premioLvlUp = marioObj.getProperties();

		MapObject champiObj = tma.getRectangleList("champi").get(0);
		this.premioChampi = champiObj.getProperties();

	}

	/**
	 * carga el raton
	 */
	private void cargarRaton() {
		raton = new Raton(Gdx.input.getX(), Gdx.input.getY(), mainStage, this);
	}

	/**
	 * carga los enemigos en el punto start segun el mapa
	 * @param o
	 */
	private void cargarEnemigos(int o) {

		if (o != 0) {
			for (int i = 0; i < (10 * this.lvl); i++) {

				int x = (int) (Math.random() * (4 - 1) + 1);

				enemigo = new Tanke((float) startProps1.get("x"), (float) startProps1.get("y"), mainStage, this, x, 1);
				tankes.add(enemigo);

			}

			this.countTankes = tankes.size();
		} else {
			for (int i = 0; i < (10 * this.lvl); i++) {

				int x = (int) (Math.random() * (4 - 1) + 1);

				enemigo = new Tanke((float) startProps1.get("x"), (float) startProps1.get("y"), mainStage, this, x, 1);
				tankes.add(enemigo);

			}

			for (int i = 0; i < (10 * this.lvl); i++) {

				int x = (int) (Math.random() * (4 - 1) + 1);

				enemigo = new Tanke((float) startProps2.get("x"), (float) startProps2.get("y"), mainStage, this, x, 2);
				tankes.add(enemigo);

			}

			this.countTankes = tankes.size();
		}

	}

	/**
	 * carga las torres en la poscion elegida
	 * @param i
	 */
	private void cargarTorres(int i) {
		PositionTorreUser torre;
		MapProperties propsTorres;

		for (MapObject obj : tma.getRectangleList("torre")) {
			propsTorres = obj.getProperties();
			torre = new PositionTorreUser((float) propsTorres.get("x"), (float) propsTorres.get("y"), mainStage,
					(float) propsTorres.get("width"), (float) propsTorres.get("height"), lvl);
			torreUser.add(torre);
		}
	}

	/**
	 * carga los caminos que seguiran los tanques y el champi�on, segun el mapa
	 * @param i
	 */
	private void cargarCaminos(int i) {

		Camino sueloChampi;
		MapProperties propsCaminoChampi;

		for (MapObject obj : tma.getRectangleList("champi")) {
			propsCaminoChampi = obj.getProperties();
			sueloChampi = new Camino((float) propsCaminoChampi.get("x"), (float) propsCaminoChampi.get("y"), mainStage,
					(float) propsCaminoChampi.get("width"), (float) propsCaminoChampi.get("height"),
					(int) propsCaminoChampi.get("valor"));
			caminoChampi.add(sueloChampi);
		}

		if (i != 0) {
			Camino suelo;
			MapProperties propsCamino;

			for (MapObject obj : tma.getRectangleList("camino")) {
				propsCamino = obj.getProperties();
				suelo = new Camino((float) propsCamino.get("x"), (float) propsCamino.get("y"), mainStage,
						(float) propsCamino.get("width"), (float) propsCamino.get("height"),
						(int) propsCamino.get("valor"));
				camino1.add(suelo);
			}
		} else {
			Camino suelo1;
			MapProperties propsCamino1;

			for (MapObject obj : tma.getRectangleList("casilla1")) {
				propsCamino1 = obj.getProperties();
				suelo1 = new Camino((float) propsCamino1.get("x"), (float) propsCamino1.get("y"), mainStage,
						(float) propsCamino1.get("width"), (float) propsCamino1.get("height"),
						(int) propsCamino1.get("valor"));
				camino1.add(suelo1);
			}

			Camino suelo2;
			MapProperties propsCamino2;
			for (MapObject obj : tma.getRectangleList("casilla2")) {
				propsCamino2 = obj.getProperties();
				suelo2 = new Camino((float) propsCamino2.get("x"), (float) propsCamino2.get("y"), mainStage,
						(float) propsCamino2.get("width"), (float) propsCamino2.get("height"),
						(int) propsCamino2.get("valor"));
				camino2.add(suelo2);
			}
		}
	}

	// selecciona el punto start que se debe cargar
	private void cargarPuertoStart(int i) {

		if (i != 0) {
			puntoStart1("start");
		} else {
			puntoStart2("start1", "start2");
		}

	}

	// carga el punto start2
	private void puntoStart2(String start1, String start2) {
		MapObject startPoint1 = tma.getRectangleList(start1).get(0);
		startProps1 = startPoint1.getProperties();
		MapObject startPoint2 = tma.getRectangleList(start2).get(0);
		startProps2 = startPoint2.getProperties();

	}

	// carga el punto start1
	private void puntoStart1(String start) {
		MapObject startPoint1 = tma.getRectangleList(start).get(0);
		startProps1 = startPoint1.getProperties();
	}

	// selecciona el mapa
	private void cargarMapa(int i) {

		if (i != 0) {
			mapa("assets/mapa/mapa.tmx");
		} else {
			mapa("assets/mapa/mapa1.tmx");
		}

	}

	// carga el TilemapActor
	private void mapa(String mapa) {
		tma = new TilemapActor(mapa, mainStage);

	}
	
	// carga la camara
	private void cargarCamara() {
		((OrthographicCamera) this.mainStage.getCamera()).setToOrtho(false, Const.anchura, Const.altura);

	}

	@Override
	public void update(float dt) {

		// controla que torre se a eleguido
		if (Gdx.input.isKeyPressed(Keys.Q)) {
			this.torreElegida = 1;
		}

		if (Gdx.input.isKeyPressed(Keys.W)) {
			this.torreElegida = 2;
		}

		if (Gdx.input.isKeyPressed(Keys.E)) {
			this.torreElegida = 3;
		}

		// controla la activacion o desactivacion de la musica
		if (Gdx.input.isKeyPressed(Keys.S) && musica) {
			theme.pause();
			musica = false;
		} else if (Gdx.input.isKeyPressed(Keys.S) && !musica) {
			theme.play();
			musica = true;
		}

		// controla el posicionamiento de las torres
		if (Gdx.input.isTouched() && this.torreElegida != 0 && this.coint > 0) {
			mirarPosicionesTorre();
		}

		// controla el despliege de los tanques
		if (this.tiempoTranscurrido <= 0 && (this.tankesTotalesDesplegados < this.tankes.size())) {

			this.tankes.get(this.tankesTotalesDesplegados).torre.vivo = true;
			this.tankesTotalesDesplegados += 1;

			this.tiempoTranscurrido = this.tiempoDespliege;

		} else if (this.tankesTotalesDesplegados < this.tankes.size()) {

			this.tiempoTranscurrido -= dt;

		}

		// controla is se a pinchado en la moneda
		if (Gdx.input.isTouched() && this.moneda.vivo && this.raton.overlaps(this.moneda)) {

			this.raton.overlaps(this.moneda);

			this.moneda.vivo = false;

			this.coint += 30;
		}
		
		// carga a mario
		cargaMario(dt);
		// carga al champi�on
		cargaChampi(dt);
		// controla el fin del juego
		controlFinJuego();

	}

	/**
	 * Controla las interacciones entre el champi�on y el raton
	 * 
	 * @param dt
	 */
	private void cargaChampi(float dt) {
		// controla si se a presionado al chmpi�on con el raton
		if (Gdx.input.isTouched() && this.champi.vivo && this.raton.overlaps(this.champi)) {

			this.champi.vivo = false;

			this.controlChampi = true;

			this.timeChampi = this.controlTimeChampi;
			// activa la advertencia del buffo
			extra = new Label("Max speed tower", BaseGame.labelStyle);
			uiStage.addActor(extra);
			extra.setPosition(Const.anchura / 3, Const.altura / 2);

			for (TorretaUser aux : this.torretaUser) {
				// activa el buffo
				aux.tiempoDisparo = aux.tiempoDisparo * 2;

			}
		}

		// controla la activacion del champi�on
		if (this.tankesDeadInLvl > 10 && !this.champi.vivo && !this.controlChampi) {
			this.champi.vivo = true;
		}
		// controla si esta activo el bufo
		if (this.timeChampi > 0) {
			this.timeChampi -= dt;
		}
		// controla si el tiempo se a acabado
		if (this.controlChampi && this.timeChampi <= 0 && !this.controlDeadChampi) {
			// controla que solo se active una vez
			this.controlDeadChampi = true;
			for (TorretaUser aux : this.torretaUser) {
				// quita la advertencia
				extra.remove();
				// quita el bufo
				aux.tiempoDisparo = aux.tiempoDisparo / 2;

			}
		}

	}

	/**
	 * Controla las interacciones entre el mario y el raton
	 * 
	 * @param dt
	 */
	private void cargaMario(float dt) {

		// controla si se a presionado al mario con el raton
		if (Gdx.input.isTouched() && this.mario.vivo && this.raton.overlaps(this.mario)) {

			this.mario.vivo = false;

			this.controlMario = true;

			this.timeMario = this.controlTimeMario;

			// advierte del bufo
			extra = new Label("Max level tower", BaseGame.labelStyle);
			uiStage.addActor(extra);
			extra.setPosition(Const.anchura / 3, Const.altura / 2);

			for (TorretaUser aux : this.torretaUser) {
				// agrega el bufo
				aux.lvl += 2;

			}
		}

		// controla el momento de activacion
		if (this.tankesDeadInLvl > 3 && !this.mario.vivo && !this.controlMario) {
			this.mario.vivo = true;
		}

		// controla el tiempo de mario
		if (this.timeMario > 0) {
			this.timeMario -= dt;
		}

		// controla si se a acabdo del tiempo del bufo
		if (this.controlMario && this.timeMario <= 0 && !this.controlDeadMario) {

			// controla que solo se ejecuta una vez por pantalla
			this.controlDeadMario = true;
			// la advertencia
			extra.remove();
			// quita el bufo
			for (TorretaUser aux : this.torretaUser) {

				aux.lvl -= 2;

			}
		}

	}

	/**
	 * comprueba si es pocible agregar una torre en el lugar seleccionado por el
	 * jugador
	 */
	private void mirarPosicionesTorre() {
		int x = Gdx.input.getX();
		int y = (Gdx.graphics.getHeight() - (Gdx.input.getY()) - 24);

		for (PositionTorreUser aux2 : torreUser) {
			if (aux2.enUso == false) {
				if ((int) aux2.getX() <= x && aux2.getY() >= y) {
					if ((int) aux2.getX() + 32 >= x && aux2.getY() - 32 <= y) {

						agregarTorre(aux2);

						break;
					}
				}
			}
		}

	}

	/**
	 * carga la torre segun la elecciond del jugador
	 * 
	 * @param aux2
	 */
	private void agregarTorre(PositionTorreUser aux2) {

		Sound sonido1;
		Sound sonido2;
		int vida;
		int dano;
		float velocidad;
		TorretaUser aux;

		switch (this.torreElegida) {

		case 1:
			if (this.coint >= Const.PRICE_T_1) {

				sonido1 = (Sound) Gdx.audio
						.newSound(Gdx.files.internal("assets/musica/heavy-machine-gun meloboom.mp3"));
				sonido2 = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/musica/shoot meloboom.mp3"));

				vida = 80;
				dano = 80;
				velocidad = (float) 3;

				aux = new TorretaUser((float) aux2.getX(), (float) aux2.getY(), this.mainStage, this, aux2, vida, dano,
						velocidad, 1, volumen, sonido1, sonido2);

				this.torretaUser.add(aux);

				aux2.enUso = true;

				this.coint -= Const.PRICE_T_1;

			}
			break;
		case 2:
			if (this.coint >= Const.PRICE_T_2) {

				sonido1 = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/musica/laser-gun meloboom.mp3"));
				sonido2 = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/musica/laser-message meloboom.mp3"));

				vida = 60;
				dano = 100;
				velocidad = (float) 2;

				aux = new TorretaUser((float) aux2.getX(), (float) aux2.getY(), this.mainStage, this, aux2, vida, dano,
						velocidad, 2, volumen, sonido1, sonido2);

				this.torretaUser.add(aux);

				aux2.enUso = true;
				this.coint -= Const.PRICE_T_2;
			}
			break;
		case 3:
			if (this.coint >= Const.PRICE_T_3) {
				String[] imgMov3 = { "assets/torretasUser/torreta3/nivel1/tile008.png" };

				String[] img3 = { "assets/torretasUser/torreta3/nivel1/tile008.png" };

				sonido1 = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/musica/armor-piercing meloboom.mp3"));
				sonido2 = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/musica/bomb-fall meloboom.mp3"));

				vida = 100;
				dano = 150;
				velocidad = (float) 6;

				aux = new TorretaUser((float) aux2.getX(), (float) aux2.getY(), this.mainStage, this, aux2, vida, dano,
						velocidad, 3, volumen, sonido1, sonido2);

				this.torretaUser.add(aux);

				aux2.enUso = true;
				this.coint -= Const.PRICE_T_3;
			}
			break;

		}

	}

	/**
	 * Controla si el juego se a finalizado o no
	 */
	private void controlFinJuego() {

		if (this.countTankes <= 0 && vida > 0) {
			this.controlMario = false;
			this.controlChampi = false;
			cargarNewLvl();
			this.dispose();
		}

		if (vida <= 0) {

			gameOver();
			this.dispose();

		}

	}

	/**
	 * Carga el BaseScreen del siguienten nivel
	 */
	private void cargarNewLvl() {

		this.theme.dispose();

		BaseGame.setActiveScreen(
				new AccionesVentana(this.vida, this.lvl + 1, this.tankesDead, this.coint, this.musica));

	}

	/**
	 * carga el BaseScreen de GameOver
	 */
	private void gameOver() {

		this.theme.dispose();

		BaseGame.setActiveScreen(new GameOver());

	}

}
