package com.albert.escenario;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import Base.BaseActor;

/**
 * Clase PositionTorreUser, encarga de localizar las posiciones habilitadas para posicionar las torres y de indicar si estan siendo usadas o no
 * @author cloud
 *
 */
public class PositionTorreUser extends BaseActor {
	//Atributos
	public boolean enUso; // controla si la posicion esta en uso o no

	public PositionTorreUser(float x, float y, Stage s,  float width, float height,
			int lvl) {
		super(x, y, s);
		
		enUso = false;

		setSize(width, height);

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		super.draw(batch, parentAlpha);


	}

}
