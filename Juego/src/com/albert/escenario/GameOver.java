package com.albert.escenario;


import com.alberto.constantes.Const;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import Base.BaseGame;
import Base.BaseScreen;

/**
 * Clase GameOver, se encarga de finalizar el juego al quedarse el jugador sin vida
 * 
 * @author cloud
 *
 */
public class GameOver extends BaseScreen {
	private Label mensaje;
	private Label mensaje2;
	private Decoracion decoracion;

	/**
	 * Metodo que invoca los disintos elementos que vera el jugador al finalizar el juego
	 */
	public GameOver() {

		// TODO Auto-generated method stub

		mensaje = new Label("Has perdido", BaseGame.labelStyle);
		mensaje2 = new Label("Press ESC", BaseGame.labelStyle);
		

		String[] imagen = {"assets\\decoracion\\MetalSlurg.png"};
		decoracion = new Decoracion(Const.altura / 2, Const.anchura / 2, mainStage, imagen);

		uiStage.addActor(mensaje);
		mensaje.setPosition(Const.anchura / 3, Const.altura / 2);
		uiStage.addActor(mensaje2);
		mensaje2.setPosition(Const.anchura / 3,Const.altura-50);
		
		uiStage.addActor(decoracion);
		decoracion.setPosition(Const.anchura / 3, Const.altura / 10);
		

	}

	@Override
	public void initialize() {
	

	}
	

	@Override
	public void update(float dt) {
		// si presiona la tecla ESC vuelve al inicio
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			this.dispose();

			BaseGame.setActiveScreen(new Inicio());
		}
	}

}
