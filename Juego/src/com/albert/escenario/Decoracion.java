package com.albert.escenario;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase Decoracion, clase para uso no especifico. Pricipalmente para la decoracion en pantalla GameOver
 * @author cloud
 *
 */
public class Decoracion extends BaseActor{

	private Animation animation;
	
	public Decoracion(float x, float y, Stage s, String[] imagen) {
		super(x, y, s);
		this.animation = loadAnimationFromFiles(imagen, 0.5f, true);
	}

	public void act() {
		
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		super.draw(batch, parentAlpha);
	}

	


}
