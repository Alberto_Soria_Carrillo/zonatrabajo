package com.albert.escenario;

import Base.BaseGame;

/**
 * Clase VentanaJuego, se encarga de carga la ventana del juego
 * @author cloud
 *
 */
public class VentanaJuego extends BaseGame {

	/*
	 * Crea la ventana del juego
	 */
	public void create() {
		super.create();
		setActiveScreen(new Inicio());

	}
}
