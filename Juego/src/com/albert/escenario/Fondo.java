package com.albert.escenario;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;

import Base.BaseActor;

/**
 * Clase Fondo, se encarga de cargar la imagen de fondo al inicio del programa
 * @author cloud
 *
 */
public class Fondo extends BaseActor {
	OrthographicCamera cam, camNivel;
	Stage s;
	Inicio nivel;
	float x;

	public Fondo(float x, float y, Stage s, Inicio nivel) {
		super(x, y, s);
		
		// carga la imagen segun un numero aleatorio
		x = (float) Math.random(); 

		if (x >= 0.5) {
			loadTexture("assets\\decoracion\\186.png");
		} else if (x < 0.5) {
			loadTexture("assets\\decoracion\\community_image_1399026909.png");
		}
		
		this.s = s;
		this.nivel = nivel;

	}

	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub
		super.act(dt);

	}



}
