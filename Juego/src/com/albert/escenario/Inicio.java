package com.albert.escenario;

import com.alberto.constantes.Const;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import Base.BaseGame;
import Base.BaseScreen;

/**
 * Clase Inicio, se encarga de cargar la ventana de inicio del juego
 * @author cloud
 *
 */
public class Inicio extends BaseScreen{

	private Table table;
	@Override
	public void initialize() {
		// carga la camara
		((OrthographicCamera) this.mainStage.getCamera()).setToOrtho(false, Const.anchura, Const.altura);
		// carga el fondo aleatorio
		Fondo fondo = new Fondo(0, 0, backStage, this);
		
		// crea la table donde se posicionaran los elementos
		table=new Table();
		table.setFillParent(true);
		this.uiStage.addActor(table);
		
		// titulo
		Label titulo=new Label("Tankes Locos",BaseGame.labelStyle);
		table.add(titulo);
		
		table.row();
		
		// Boton de jugar
		TextButton boton=new TextButton("Jugar", BaseGame.textButtonStyle);
		boton.addListener(
				(Event e)->{if(!(e instanceof InputEvent)|| !((InputEvent)e).getType().equals(Type.touchDown))
					return false;
				this.dispose();
				BaseGame.setActiveScreen(new AccionesVentana(Const.VIDA, 1, 0,Const.COINT, true));
				return false;
				});
		table.add(boton);
		
		table.row();
		
		// boton de salir
		TextButton botonSalir=new TextButton("Salir", BaseGame.textButtonStyle);
		botonSalir.addListener(
				(Event e)->{if(!(e instanceof InputEvent)|| !((InputEvent)e).getType().equals(Type.touchDown))
					return false;
				this.dispose();
				Gdx.app.exit();
				return false;
				});
		table.add(botonSalir);
		
		table.row();
		
		// Texto indicando compo se actiba o se desactiva la musica
		Label musica=new Label("Press \"S\" on off music",BaseGame.labelStyle);
		table.add(musica);
		
		
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}
	


}
