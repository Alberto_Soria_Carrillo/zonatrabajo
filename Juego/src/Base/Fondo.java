package Base;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Fondo extends BaseActor {
	OrthographicCamera cam, camNivel;
	Stage s;
	LevelScreen nivel;

	public Fondo(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s);
		// TODO Auto-generated constructor stub
		if (Parametros.nivel == 1) {
			loadTexture("assets/mapas/white-cloud-blue-sky.jpg");
		} else if (Parametros.nivel == 2) {
			loadTexture("assets/mapas/bg-cave.png");
		} else if (Parametros.nivel == 3) {
			loadTexture("assets/mapas/bg-snow.png");
		}
		this.s = s;
		this.nivel = nivel;

	}

	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub
		super.act(dt);
		alignCamera();
	}

	public void alignCamera() {
		Camera cam = this.getStage().getCamera();
		Viewport v = this.getStage().getViewport();

		// center camera on actor
		// cam.position.set( this.getX() + this.getOriginX(), this.getY() +
		// this.getOriginY(), 0 );
		float alturaPersonaje = nivel.mainStage.getCamera().position.y / BaseActor.getWorldBounds().height;
		float anchuraPersonaje = nivel.mainStage.getCamera().position.x / BaseActor.getWorldBounds().width;

		cam.position.x = this.getWidth() * anchuraPersonaje;
		cam.position.y = this.getHeight() * alturaPersonaje;

		// bound camera to layout
		cam.position.x = MathUtils.clamp(cam.position.x, cam.viewportWidth / 2,
				this.getWidth() - cam.viewportWidth / 2);
		cam.position.y = MathUtils.clamp(cam.position.y, cam.viewportHeight / 2,
				this.getHeight() - cam.viewportHeight / 2);

		cam.update();
	}

}
