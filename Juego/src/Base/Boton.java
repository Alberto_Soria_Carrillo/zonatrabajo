package Base;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Boton extends BaseActor {
	public boolean pulsado;
	private Animation p;

	public Boton(float x, float y, Stage s) {
		super(x, y, s);
		// TODO Auto-generated constructor stub
		pulsado = false;
		loadTexture("assets/items/switchRed.png");
		p = loadTexture("assets/items/switchRed_pressed.png");

	}

	public void pulsar() {
		if (!pulsado) {
			pulsado = true;

			setAnimation(p);

		}
	}

}
