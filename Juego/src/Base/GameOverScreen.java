package Base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

public class GameOverScreen extends BaseScreen {
	private Label mensaje;

	@Override
	public void initialize() {

		// TODO Auto-generated method stub

		mensaje = new Label("", BaseGame.labelStyle);
		if (!Parametros.fin) {
			mensaje.setText("�Muy bien!");
		} else {
			mensaje.setText("Otra vez ser�");

		}

		uiStage.addActor(mensaje);
		mensaje.setPosition(500, 400);
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			this.dispose();

			BaseGame.setActiveScreen(new LevelScreen());
		}

	}

}
