package Base;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Bala extends BaseActor {
	// Proyectil disparado por el jugador
	private LevelScreen nivel;

//Atributos para gestionar la vida del proyectil
	private float tiempoBala; // tiempo que el proyectil permanecer� en pantalla una vez disparado
	private float velocidadBala; // velocidad a la que se mueve el proyectil
	private float cuentaBala;// variable para llevar la cuenta del tiempo que la bala lleva en pantalla
	private int direccion;// 1= derecha -1=izquierda

	private boolean enabled;

	public Bala(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s);

		this.nivel = nivel;
		// iniciar variables de control
		tiempoBala = 10;
		velocidadBala = 400;

		// cargar textura del proyectil
		loadTexture("assets/items/fireball.png");
	}

	@Override
	public void act(float dt) {

		if (enabled) {// solo calcularemos si el proyecti est� activo
			super.act(dt);
			this.rotateBy(-10 * direccion); // el proyectil rota en un sentido u otro dependiendo de su direcci�n
			moveBy(velocityVec.x * dt, velocityVec.y * dt);// actualizar posici�n del proyectil seg�n su velocidad
			cuentaBala -= dt; // actualizamos el tiempo que le queda en pantalla
			
			for (Enemigo enemigo : nivel.enemigos) {// miramos si el proyectil ha impactado en alg�n enemigo
				if (enemigo.vivo && this.enabled && this.overlaps(enemigo)) {
					enemigo.dano();// ejecutamos la funci�n de da�o del enemigo impactado
					this.enabled = false; // desactivamos la bala tras el impacto
				}
			}

			if (cuentaBala <= 0) {// si se ha acabado el tiempo en pantalla desactivo la bala
				enabled = false;
			}

		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (enabled)// solo pinto el proyectil si ha sido disparado/est� activo
			super.draw(batch, parentAlpha);
	}

	public void disparar(int direccion) {
		// metodo que se ejecuta al disparar este proyectil

		this.enabled = true;// activo el proyectil
		this.setPosition(nivel.jugador.getX(), nivel.jugador.getY());// fijo la posici�n inicial del proyectil en el
																		// personaje que lo ha disparado
		cuentaBala = tiempoBala;// empiezo la cuenta del tiempo que permanecer� en pantalla
		this.velocityVec.set(velocidadBala * direccion, 0);// le doy una velocidad dependiente de la direcci�n en la que
															// se haya disparado
		this.direccion = direccion;
	}

}
