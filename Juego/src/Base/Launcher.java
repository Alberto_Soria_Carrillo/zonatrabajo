package Base;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

public class Launcher {
	public static void main(String[] args) {
		Game myGame = new Jueguecillo();
		LwjglApplication launcher = new LwjglApplication(myGame, "Nave Chunga", Parametros.anchura, Parametros.altura);
	}
}