package com.alberto.game.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class ConfigurationManager {
    public static Preferences prefs = Gdx.app.getPreferences("Invaders");

    public static boolean haySonido() {
        return prefs.getBoolean("sonido");
    }

    public static String getDificultadd() {
        return prefs.getString("dificultad");
    }
    public static int getDificultad() {
        String s = prefs.getString("dificultad");
        if (s.equals("Fácil")) {
            return 1;
        }
        if (s.equals("Normal"))
            return 2;
        if (s.equals("Dificil"))
            return 3;
        return 0;
    }
}
