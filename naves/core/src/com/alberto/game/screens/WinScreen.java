package com.alberto.game.screens;

import com.alberto.game.MainGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.alberto.game.screens.AbstractScreen;

public class WinScreen extends AbstractScreen {
    private Texture TexturaFondo;
    private int contador; // Para que no se pueda pulsar desde el principio la pantalla.

    public WinScreen(MainGame mainGame) {
        super(mainGame);
    }

    @Override
    public void show() {
        TexturaFondo = mainGame.getManager().get("data/Win.png", Texture.class);
        contador = 100;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if(Gdx.input.isTouched() && contador == 0)
            mainGame.setScreen(mainGame.PRINCIPAL); // Te devuelve a la pantalla del menú
        else if(contador > 0)
            contador--;

        batch.begin();
        batch.draw(TexturaFondo, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }
    @Override
    public void hide() {

    }
    @Override
    public void pause() {

    }
    @Override
    public void resume() {

    }
    @Override
    public void dispose() {

    }
}
