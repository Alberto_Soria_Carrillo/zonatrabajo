package com.alberto.game.screens;

import com.alberto.game.MainGame;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisSelectBox;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.alberto.game.screens.AbstractScreen;
import com.alberto.game.screens.MainScreen;

public class ConfigurationScreen extends AbstractScreen {
    private Texture texturaFondo;
    private Stage stage;
    private Preferences prefs;
    private VisCheckBox checkSonido;
    private VisSelectBox<String> sbDificultad;

    public ConfigurationScreen(MainGame mainGame) {
        super(mainGame);
        texturaFondo = mainGame.getManager().get("data/Background.png", Texture.class); // Asociamos la textura con la imagen correspondiente

        cargarConfiguracion();

    }



    private void cargarConfiguracion() {
        prefs = Gdx.app.getPreferences("Invaders");
    }

    private void grabarConfiguracion() {

        prefs.putBoolean("sonido", checkSonido.isChecked());
        prefs.putString("dificultad", sbDificultad.getSelected());
        prefs.flush();
    }

    @Override
    public void show() {

        if (!VisUI.isLoaded())
            VisUI.load();

        stage = new Stage();

        VisTable table = new VisTable(true);
        table.setFillParent(true);
        stage.addActor(table);

        checkSonido = new VisCheckBox("SONIDO");
        checkSonido.setChecked(prefs.getBoolean("sonido", false));

        sbDificultad = new VisSelectBox<String>();
        Array<String> items = new Array<String>();
        items.add("Fácil");
        items.add("Normal");
        items.add("Dificil");
        sbDificultad.setItems(items);
        sbDificultad.setSelected(prefs.getString("dificultad", "Normal"));

        VisTextButton quitButton = new VisTextButton("VOLVER");
        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                grabarConfiguracion();
                dispose();
                ((Game) Gdx.app.getApplicationListener()).
                        setScreen(new MainScreen(mainGame));

            }
        });
        VisTextButton insbtl = new VisTextButton("<<<<Flechas Izquierda y derechapara mover>>>>");
        VisTextButton insbtspace = new VisTextButton("Tecla space para disparar y esc para volver al menu");

        // Añade filas a la tabla y añade los componentes
        table.row();
        table.add(checkSonido).center().width(200).height(100).pad(5);
        table.row();
        table.add(sbDificultad).center().width(200).height(50).pad(5);
        table.row();
        table.add(quitButton).center().width(200).height(50).pad(5);
        table.row();
        table.add(insbtl).center().width(500).height(20).pad(5);
        table.row();
        table.add(insbtspace).center().width(500).height(20).pad(5);


        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float dt) {
        //Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(texturaFondo,0,0);
        batch.end();
        // Pinta la UI en la pantalla
        stage.act(dt);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
