package com.alberto.game.screens;

import com.alberto.game.MainGame;
import com.alberto.game.bottons.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.alberto.game.bottons.Boton;
import com.alberto.game.bottons.BotonConfiguration;
import com.alberto.game.bottons.BotonExit;
import com.alberto.game.bottons.BotonPlay;
import com.alberto.game.bottons.BotonRecords;
import com.alberto.game.screens.AbstractScreen;

import java.util.ArrayList;
import java.util.List;

public class MainScreen extends AbstractScreen {
    private List<Boton> botones;

    public MainScreen(MainGame mainGame) {
        super(mainGame);

        botones = new ArrayList<Boton>();
        int enMedioDeLaPantalla = Gdx.graphics.getWidth() / 2 - mainGame.getManager().get("data/BotonPlay.png", Texture.class).getWidth() / 2;
        botones.add(new BotonPlay(mainGame, new Vector2(enMedioDeLaPantalla, Gdx.graphics.getHeight() / 2 + 50)));
        botones.add(new BotonRecords(mainGame, new Vector2(enMedioDeLaPantalla, Gdx.graphics.getHeight() / 2 - 60)));
        botones.add(new BotonExit(mainGame, new Vector2(enMedioDeLaPantalla, Gdx.graphics.getHeight() / 2 - 170)));
        botones.add(new BotonConfiguration(mainGame,new Vector2(Gdx.graphics.getWidth()-50,Gdx.graphics.getHeight() -50)));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        for(Boton boton : botones)
            boton.update();

        batch.begin();
        batch.draw(mainGame.getManager().get("data/Background.png", Texture.class), 0, 0,
                Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        for(Boton boton : botones)
            boton.draw(batch);
        batch.end();
    }

    @Override
    public void show() {

    }
    @Override
    public void hide() {
    }
    @Override
    public void dispose() {
    }
}
