package com.alberto.game.screens;

import com.alberto.game.MainGame;
import com.alberto.game.charapters.*;
import com.alberto.game.manager.ConfigurationManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.math.Vector2;
import com.alberto.game.charapters.Alien;
import com.alberto.game.charapters.AlienBonus;
import com.alberto.game.charapters.Ball;
import com.alberto.game.charapters.Explosion;
import com.alberto.game.charapters.MegaShotShip;
import com.alberto.game.charapters.Ship;
import com.alberto.game.charapters.Shot;
import com.alberto.game.charapters.ShotAlien;
import com.alberto.game.charapters.ShotShip;
import com.alberto.game.screens.AbstractScreen;

import javax.lang.model.type.ArrayType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class GameScreen extends AbstractScreen{//todo preparar pause

    private static final float LIMITE_DISPARO = Gdx.graphics.getHeight();
    private static final float MAXIMO_ALIENS_FILA = 8;
    private static final float DISTANCIA = 50; // Distancia en el eje x donde se van colocando los aliens.
    private static final float ALTURA = Gdx.graphics.getHeight() - 70; // Distancia en el eje y donde empiezan los aliens.
    private static final float MARGEN_IZQUIERDO = 50;
    private static final float MARGEN_DERECHO = Gdx.graphics.getWidth();
    private static final float SPEED = 1.2f;
    private static final float AUMENTO_DE_VELOCIDAD = 1.5f;
    private static final float FRECUENCIA_ALIEN_BONUS = 4 * ConfigurationManager.getDificultad();


    private Texture texturaFondo;
    private Texture texturaAlien1, texturaAlien2, texturaAlien3, texturaAlien4;
    private Ship nave;

    private ArrayList<Alien> aliensTipo1;
    private ArrayList<Alien> aliensTipo2;
    private ArrayList<Alien> aliensTipo3;
    private ArrayList<Alien> aliensTipo4;
    private Array<Texture> explosionTexture;
    private Array<Explosion> explosiones;

    private Shot disparoNave;
    private Shot disparoAlien;
    private boolean actualizarDisparoNave;
    private boolean actualizarDisparoAlien;
    private boolean gameOver;
    private int marcadorDePuntos;
    private int nivel ;
    private float velocidadAliens;//aumento
    private Random aleatorio; // para disparo aliens
    private int vidasNave;

    private Ball bola; // Bola poweup
    private Random random; //para poweup
    private boolean megaDisparo;

    private boolean naveBonus;
    private AlienBonus alienBonus;

    public GameScreen(MainGame mainGame) {
        super(mainGame);
    }

    @Override
    public void show() {
        texturaFondo = mainGame.getManager().get("data/Background.png", Texture.class);
        texturaFondo.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        nave = new Ship(mainGame, new Vector2(10,10));

        aliensTipo1 = new ArrayList<Alien>();
        aliensTipo2 = new ArrayList<Alien>();
        aliensTipo3 = new ArrayList<Alien>();
        aliensTipo4 = new ArrayList<Alien>();
        explosionTexture = new Array<Texture>();
        explosiones = new Array<Explosion>();
        for (int i = 1; i <= 17; i++) {
            Texture exp = mainGame.getManager().get("animated/explosion-" + i + ".png",Texture.class);
            explosionTexture.add(exp);
        }

        velocidadAliens = SPEED;

        crearAliens();

        aleatorio = new Random();
        gameOver = false;
        marcadorDePuntos = 0;
        nivel = 1;
        vidasNave = 3;

        random = new Random();
        bola = new Ball(mainGame, new Vector2(random.nextInt(Gdx.graphics.getWidth() -
                mainGame.getManager().get("data/bolaBalaEspecial.png", Texture.class).getWidth()),
                Gdx.graphics.getHeight()));

        naveBonus = false;
    }
    private void updateExplosion(float delta) {

        for (Explosion exp : explosiones) {
            exp.atualizar(delta);
            // verifica se a explosão já passou todos os estágios
            if (exp.getEstado() > 17) {
                explosiones.removeValue(exp, true);
            }
        }
    }
    private void criarExplosao(float x, float y) {
        // a imagem da explosão mede 96 por 96 pixels
        Explosion exp = new Explosion();
        exp.setX(x - 96 / 2);
        exp.setY(y - 96 / 2);
        explosiones.add(exp);
    }
    private void crearAliens() {
        //  tipo1
        texturaAlien1 = mainGame.getManager().get("data/alien1.png", Texture.class);
        texturaAlien1.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        float altura = ALTURA;
        posicionarAliens(aliensTipo1, texturaAlien1, altura);
        altura = reducirAlturaAliens(texturaAlien1, altura);

        // aliens tipo2
        texturaAlien2 = mainGame.getManager().get("data/alien2.png", Texture.class);
        texturaAlien2.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        posicionarAliens(aliensTipo2, texturaAlien2, altura);
        altura = reducirAlturaAliens(texturaAlien2, altura);

        //aliens tipo3
        texturaAlien3 = mainGame.getManager().get("data/alien3.png", Texture.class);
        texturaAlien3.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        posicionarAliens(aliensTipo3, texturaAlien3, altura);
        altura = reducirAlturaAliens(texturaAlien3, altura);

        //aliens tipo4
        texturaAlien4 = mainGame.getManager().get("data/alien4.png", Texture.class);
        texturaAlien4.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        posicionarAliens(aliensTipo4, texturaAlien4, altura);

        actualizarDisparoNave = false;
        actualizarDisparoAlien = false;
    }

    private void posicionarAliens(ArrayList<Alien> aliens, Texture texturaAlien, float altura) {
        float distancia = DISTANCIA;
        float limiteDerecha = MARGEN_DERECHO - ((texturaAlien.getWidth() + 5) * MAXIMO_ALIENS_FILA);
        float limiteIzquierda = MARGEN_IZQUIERDO;
        for(int i = 0; i < MAXIMO_ALIENS_FILA ; i++) {
            aliens.add(new Alien(texturaAlien, new Vector2(distancia, altura), limiteIzquierda, limiteDerecha, velocidadAliens));
            distancia = distancia + texturaAlien.getHeight() + 5;
            limiteDerecha = limiteDerecha + texturaAlien.getHeight() + 5;
            limiteIzquierda = limiteIzquierda + texturaAlien.getHeight() + 5;
        }
    }

    private float reducirAlturaAliens(Texture texturaAlien, float altura) {
        return altura - texturaAlien.getHeight() - 5;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        volverAlMenu();

        boolean naveDispara = nave.update(); // true si se ha disparado.

        disparoNaveUpdate(naveDispara,delta);
        disparoAlienUpdate(delta);

        bolaUpdate(delta);

        aliensUpdate(aliensTipo1,delta);
        aliensUpdate(aliensTipo2,delta);
        aliensUpdate(aliensTipo3,delta);
        aliensUpdate(aliensTipo4,delta);
        animatedAlien(aliensTipo1,"1");
        animatedAlien(aliensTipo2,"2");
        animatedAlien(aliensTipo3,"3");
        animatedAlien(aliensTipo4,"4");


        alienBonusUpdate();

        updateExplosion(delta);

        batch.begin();
        batch.draw(texturaFondo, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()); //La dibujamos en la esquina inferior derecha, tamaño natural
        nave.draw(batch);

        bola.draw(batch);

        pintarAliens(aliensTipo1, texturaAlien1);
        pintarAliens(aliensTipo2, texturaAlien2);
        pintarAliens(aliensTipo3, texturaAlien3);
        pintarAliens(aliensTipo4, texturaAlien4);

        if(naveBonus)
            alienBonus.draw(batch);

        if(actualizarDisparoNave)
            disparoNave.draw(batch);
        if(actualizarDisparoAlien)
            disparoAlien.draw(batch);

        mainGame.getFont().draw(batch, Integer.toString(marcadorDePuntos)+" level " + nivel, 10, Gdx.graphics.getHeight() - 10);

        for(int i = 0; i < vidasNave; i++)
            batch.draw(mainGame.getManager().get("data/ship.png", Texture.class), Gdx.graphics.getWidth() - 40 - i*35, Gdx.graphics.getHeight() - 40, 30, 30);
        for (Explosion exp : explosiones) {
            int i = exp.getEstado() - 1;
            batch.draw(explosionTexture.get(i), exp.getX(), exp.getY());
        }
        batch.end();

        comprobarFinalDelJuego();
    }

    private void bolaUpdate(float delta) {
        bola.update(delta);
        if(bola.colisionConNave(nave)) {
            megaDisparo = true; //activar el disparo especial
            bola.setPosicion(random.nextInt(Gdx.graphics.getWidth() - mainGame.getManager().get("data/bolaBalaEspecial.png", Texture.class).getWidth()), Gdx.graphics.getHeight());
        }
        else if(bola.getPosicion().y < 0 - mainGame.getManager().get("data/bolaBalaEspecial.png", Texture.class).getHeight())  // Si se esconde por abajo de la pantalla.
            bola.setPosicion(random.nextInt(Gdx.graphics.getWidth() - mainGame.getManager().get("data/bolaBalaEspecial.png", Texture.class).getWidth()), Gdx.graphics.getHeight());
    }

    private void disparoNaveUpdate(boolean naveDispara, float delta) {
        if(naveDispara && !actualizarDisparoNave) {
            if(megaDisparo) {
                disparoNave = new MegaShotShip(mainGame, new Vector2(nave.getPosicion().x, nave.getAltura() + 12));
                megaDisparo = false;
            }
            else
                disparoNave = new ShotShip(mainGame, new Vector2(nave.getPosicion().x + (nave.getAnchura() / 2 - 1), nave.getAltura() + 12));
            disparoNave.disparoSonido();
            actualizarDisparoNave = true;
        }

        if(actualizarDisparoNave) {
            disparoNave.update(delta);
            if(disparoNave.getPosicion().y + disparoNave.getAltura() > LIMITE_DISPARO)//disparo reset
                actualizarDisparoNave = false;
        }
    }
    private void disparoAlienUpdate(float delta) {

        if(!actualizarDisparoAlien) {
            ArrayList<Alien> filaDisparo = filaDeDisparo();
            int alienAleatorio = aleatorio.nextInt(filaDisparo.size());
            disparoAlien = new ShotAlien(mainGame, new Vector2(filaDisparo.get(alienAleatorio).getPosicion().x + (filaDisparo.get(alienAleatorio).getAnchura() / 2 - 1), filaDisparo.get(alienAleatorio).getPosicion().y - 5));
            disparoAlien.disparoSonido();
            actualizarDisparoAlien = true;
        }
        if(actualizarDisparoAlien) {
            disparoAlien.update(delta);
            if(nave.tocadoPorDisparo(disparoAlien)) {
                // quitar vidas nave
                disparoAlien.naveTocada();
                criarExplosao(nave.getPosicion().x + nave.getAnchura()/ 2, nave.getPosicion().y+
                        nave.getAltura() / 2);
                nave.volverAPosicionInicial();
                vidasNave--;
                if(vidasNave == 0)
                    gameOver = true;
            }
            else if(disparoAlien.getPosicion().y < 0) {
                actualizarDisparoAlien = false;
            }
        }
    }

    private ArrayList<Alien> filaDeDisparo() {
        if(aliensTipo4.isEmpty()) {
            if(aliensTipo3.isEmpty()) {
                if(aliensTipo2.isEmpty()) {
                    return aliensTipo1;
                }
                else return aliensTipo2;
            }
            else return aliensTipo3;
        }
        else return aliensTipo4;
    }
    private void animatedAlien(ArrayList<Alien> aliens, String tipo){
        for(int i = 0; i < aliens.size(); i++) {
            if (aliens.get(i).getMovimiento() == 1) {
                aliens.get(i).setTexturaAlien(mainGame.getManager().get("data/alien"+tipo+"r.png", Texture.class));
            }
            if (aliens.get(i).getMovimiento() == -1) {
                aliens.get(i).setTexturaAlien(mainGame.getManager().get("data/alien"+tipo+"l.png", Texture.class));

            }
        }

    }
    private void aliensUpdate(ArrayList<Alien> aliens, float delta) {
        for(int i = 0; i < aliens.size(); i++) {
            aliens.get(i).update(delta);
            comprobarDerrota(aliens.get(i).getBordes().y); //llega a fin pantalla
            if(actualizarDisparoNave && aliens.get(i).Muerto(disparoNave)) {
                marcadorDePuntos += 10;
                criarExplosao(aliens.get(i).getPosicion().x + aliens.get(i).getAnchura()/ 2, aliens.get(i).getPosicion().y+
                        aliens.get(i).getAltura() / 2);
                aliens.remove(i);
                disparoNave.alienMuerto();
            }
        }
    }

    private void alienBonusUpdate() {
        if(naveBonus) {
            alienBonus.update();
            if(actualizarDisparoNave && alienBonus.muerto(disparoNave)) {
                marcadorDePuntos += 100;
                criarExplosao(alienBonus.getPosicion().x + alienBonus.getAnchura()/ 2, alienBonus.getPosicion().y+
                        alienBonus.getAltura() / 2);
                naveBonus = false;
                disparoNave.alienMuerto();
            }
            else if(alienBonus.getPosicion().x > Gdx.graphics.getWidth())
                naveBonus = false;
        }

        float valorDeFrecuencia = -1;
        if(aliensTipo1.isEmpty())
            if(aliensTipo2.isEmpty())
                if(aliensTipo3.isEmpty())
                    if(aliensTipo4.isEmpty())
                        valorDeFrecuencia = -1;
                    else
                        valorDeFrecuencia = aliensTipo4.get(0).getContadorDeBajadas() % FRECUENCIA_ALIEN_BONUS;
                else
                    valorDeFrecuencia = aliensTipo3.get(0).getContadorDeBajadas() % FRECUENCIA_ALIEN_BONUS;
            else
                valorDeFrecuencia = aliensTipo2.get(0).getContadorDeBajadas() % FRECUENCIA_ALIEN_BONUS;
        else
            valorDeFrecuencia = aliensTipo1.get(0).getContadorDeBajadas() % FRECUENCIA_ALIEN_BONUS;

        if(!naveBonus && valorDeFrecuencia == 0 && aliensTipo1.get(0).getContadorDeBajadas() != 0) {
            naveBonus = true;
            alienBonus = new AlienBonus(mainGame, new Vector2(-mainGame.getManager().get("data/naveAlienBonus.png",
                    Texture.class).getWidth(), Gdx.graphics.getHeight() - mainGame.getManager().get("data/naveAlienBonus.png",
                    Texture.class).getHeight()), velocidadAliens);
        }

    }

    private void pintarAliens(ArrayList<Alien> aliens, Texture texturaAlien) {
        for(Alien alien : aliens) {
            alien.draw(batch);
        }
    }

    private void volverAlMenu() {
        if(Gdx.input.isKeyPressed(Keys.ESCAPE) || Gdx.input.isKeyPressed(Keys.BACK) || Gdx.input.isKeyPressed(Keys.MENU))
            mainGame.setScreen(mainGame.PRINCIPAL);
    }

    private boolean estanTodosLosAliensDestruidos() {
        return (aliensTipo1.isEmpty() && aliensTipo2.isEmpty() && aliensTipo3.isEmpty() && aliensTipo4.isEmpty());
    }

    private void comprobarFinalDelJuego() {
        if(estanTodosLosAliensDestruidos()) {
            velocidadAliens += AUMENTO_DE_VELOCIDAD;
            crearAliens();
            nivel++;
        }
        else if(gameOver) {
            guardarRecord(true);
            //guardarRecord();
            mainGame.setScreen(mainGame.GAMEOVER);
        }
    }

    private void guardarRecord() {//todo hacer para los 10 primeros
        //todo hacer un bucle que recorra los record los meta en array y luego los ordene y los guarde ordenados
        //todo usar R1, R2 .....
        if(marcadorDePuntos > mainGame.getPreferencias().getInteger("primerRecord", 0)) {
            mainGame.getPreferencias().putInteger("tercerRecord", mainGame.getPreferencias().getInteger("segundoRecord", 0));
            mainGame.getPreferencias().putInteger("segundoRecord", mainGame.getPreferencias().getInteger("primerRecord", 0));
            mainGame.getPreferencias().putInteger("primerRecord", marcadorDePuntos);
        }
        else if(marcadorDePuntos > mainGame.getPreferencias().getInteger("segundoRecord", 0)) {
            mainGame.getPreferencias().putInteger("tercerRecord", mainGame.getPreferencias().getInteger("segundoRecord", 0));
            mainGame.getPreferencias().putInteger("segundoRecord", marcadorDePuntos);
        }
        else if(marcadorDePuntos > mainGame.getPreferencias().getInteger("tercerRecord", 0))
            mainGame.getPreferencias().putInteger("tercerRecord", marcadorDePuntos);
        mainGame.getPreferencias().flush();
    }
    private void guardarRecord(boolean b){
        int get = 1;
        ArrayList<Integer>records = new ArrayList<Integer>();
        for (int i = 0 ; i<10; i++){
            records.add(mainGame.getPreferencias().getInteger("Rrank "+String.valueOf(get)));
            get++;
        }
        records.add(marcadorDePuntos);
        Collections.sort(records);
        records.remove(0);
        int ii= 10;
        for (int i = 0 ; i<10; i++){
            mainGame.getPreferencias().putInteger("Rrank "+String.valueOf(ii),records.get(i));
            ii--;
        }
        mainGame.getPreferencias().flush();

    }
    private void comprobarDerrota(float numero) {
        if(numero < 50) {
            gameOver = true;
        }
    }

    @Override
    public void resize(int width, int height) {

    }
    @Override
    public void hide() {


    }
    @Override
    public void pause() {

    }
    @Override
    public void resume() {

    }
    @Override
    public void dispose() {
    }
}
