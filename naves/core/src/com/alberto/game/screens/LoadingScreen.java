package com.alberto.game.screens;

import com.alberto.game.MainGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.alberto.game.screens.AbstractScreen;

public class LoadingScreen extends AbstractScreen{
    public LoadingScreen(MainGame mainGame) {
        super(mainGame);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if(mainGame.getManager().update()) {
            mainGame.cargarPantallas();
            mainGame.setScreen(mainGame.PRINCIPAL);
        }

        if(mainGame.getManager().isLoaded("data/Loading.png", Texture.class)) {
            batch.begin();
            batch.draw(mainGame.getManager().get("data/Loading.png", Texture.class), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            batch.end();
        }
    }

    @Override
    public void show() {
    }
    @Override
    public void hide() {
    }
    @Override
    public void dispose() {
    }
}
