package com.alberto.game.screens;

import com.alberto.game.MainGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.alberto.game.screens.AbstractScreen;

public class RecordScreen extends AbstractScreen{
    private float tiempoAcumulado;
    private int estadoPintar;
    private int get = 1;
    private float acumulado;
    public RecordScreen(MainGame mainGame) {
        super(mainGame);
    }
    @Override
    public void render(float delta) {
        //todo parametrizar los record tiempo acumulado a 2 s un update con delta y cambiar  los textos de lo ya creado
        //todo tiempo acumulado etc <2f ....
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        volverAlMenu(); // Permite volver al menú

        batch.begin();
        batch.draw(mainGame.getManager().get("data/Records.png", Texture.class), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        pintarTextos(delta);
        // Dibujamos las posiciones.
  /*      mainGame.getFont().setColor(Color.MAGENTA); // Permite cambiar el color de la fuente.
        mainGame.getFont().draw(batch, "RANK", Gdx.graphics.getWidth() / 2 - 150, Gdx.graphics.getHeight() / 2 + 100);
        mainGame.getFont().setColor(Color.WHITE);
        mainGame.getFont().draw(batch, "1ST", Gdx.graphics.getWidth() / 2 - 150, Gdx.graphics.getHeight() / 2 + 20);
        mainGame.getFont().draw(batch, "2ND", Gdx.graphics.getWidth() / 2 - 150, Gdx.graphics.getHeight() / 2 - 60);
        mainGame.getFont().draw(batch, "3RD", Gdx.graphics.getWidth() / 2 - 150, Gdx.graphics.getHeight() / 2 - 140);

        // Dibujamos en la pantalla los 3 mejores records
      /*  mainGame.getFont().setColor(Color.CYAN);
        mainGame.getFont().draw(batch, "SCORE", Gdx.graphics.getWidth() / 2 + 50, Gdx.graphics.getHeight() / 2 + 100);
        mainGame.getFont().setColor(Color.WHITE);
        mainGame.getFont().draw(batch, Integer.toString(mainGame.getPreferencias().getInteger("primerRecord", 0)), Gdx.graphics.getWidth() / 2 + 50, Gdx.graphics.getHeight() / 2 + 20);
        mainGame.getFont().draw(batch, Integer.toString(mainGame.getPreferencias().getInteger("segundoRecord", 0)), Gdx.graphics.getWidth() / 2 + 50, Gdx.graphics.getHeight() / 2 - 60);
        mainGame.getFont().draw(batch, Integer.toString(mainGame.getPreferencias().getInteger("tercerRecord", 0)), Gdx.graphics.getWidth() / 2 + 50, Gdx.graphics.getHeight() / 2 - 140);
*/
        batch.end();
    }
    private void pintarTextos(float delta){
        textos(get);
        acumulado += delta;
        if(acumulado>3){
            acumulado = 0;
            get++;
            if(get>8){
                System.out.println(get);
                get = 1;
            }
        }
    }
    private void textos(int get){

        // Dibujamos las posiciones.
        mainGame.getFont().setColor(Color.MAGENTA); // Permite cambiar el color de la fuente.
        mainGame.getFont().draw(batch, "RANK", Gdx.graphics.getWidth() / 2 - 150, Gdx.graphics.getHeight() / 2 + 100);
        mainGame.getFont().setColor(Color.WHITE);
        mainGame.getFont().draw(batch, "Rank "+ String.valueOf((get)), Gdx.graphics.getWidth() / 2 - 150, Gdx.graphics.getHeight() / 2 + 20);
        mainGame.getFont().draw(batch, "Rank "+String.valueOf((get+1)), Gdx.graphics.getWidth() / 2 - 150, Gdx.graphics.getHeight() / 2 - 60);
        mainGame.getFont().draw(batch, "Rank " + String.valueOf((get+ 2)), Gdx.graphics.getWidth() / 2 - 150, Gdx.graphics.getHeight() / 2 - 140);

        // Dibujamos en la pantalla los 3 mejores records
        mainGame.getFont().setColor(Color.CYAN);
        mainGame.getFont().draw(batch, "SCORE", Gdx.graphics.getWidth() / 2 + 50, Gdx.graphics.getHeight() / 2 + 100);
        mainGame.getFont().setColor(Color.WHITE);
        mainGame.getFont().draw(batch, Integer.toString(mainGame.getPreferencias().getInteger("Rrank "+ String.valueOf(get), 0)), Gdx.graphics.getWidth() / 2 + 50, Gdx.graphics.getHeight() / 2 + 20);
        mainGame.getFont().draw(batch, Integer.toString(mainGame.getPreferencias().getInteger("Rrank "+ String.valueOf((get+1)), 0)), Gdx.graphics.getWidth() / 2 + 50, Gdx.graphics.getHeight() / 2 - 60);
        mainGame.getFont().draw(batch, Integer.toString(mainGame.getPreferencias().getInteger("Rrank "+ String.valueOf((get+2)), 0)), Gdx.graphics.getWidth() / 2 + 50, Gdx.graphics.getHeight() / 2 - 140);

    }



    private void volverAlMenu() { // Método privado que tiene los botones para volver al menú durante el juego
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE) || Gdx.input.isKeyPressed(Input.Keys.BACK) || Gdx.input.isKeyPressed(Input.Keys.MENU))
            mainGame.setScreen(mainGame.PRINCIPAL);
    }

    @Override
    public void show() {

    }
    @Override
    public void hide() {

    }
    @Override
    public void dispose() {

    }
}
