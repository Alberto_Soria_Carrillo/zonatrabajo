package com.alberto.game;

import com.alberto.game.manager.ConfigurationManager;
import com.alberto.game.screens.*;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.alberto.game.screens.AbstractScreen;
import com.alberto.game.screens.ConfigurationScreen;
import com.alberto.game.screens.GameOverScreen;
import com.alberto.game.screens.GameScreen;
import com.alberto.game.screens.LoadingScreen;
import com.alberto.game.screens.MainScreen;
import com.alberto.game.screens.RecordScreen;
import com.alberto.game.screens.WinScreen;


public class MainGame extends Game {
	public AbstractScreen PRINCIPAL, GAMEOVER, WIN, LOADING, JUEGO, RECORDS, CONFIGURACION;
	private AssetManager manager;
	private SpriteBatch batch;
	private BitmapFont font;
	private Preferences preferencias;
    private ConfigurationManager configurationManager;
    boolean sonido;
    int dificultad;

	@Override
	public void create() {
		manager = new AssetManager();
		batch = new SpriteBatch();
		this.font = new BitmapFont(Gdx.files.internal("data/arial.fnt"), Gdx.files.internal("data/arial.png"), false);
        configurationManager = new ConfigurationManager();
        sonido = ConfigurationManager.haySonido();
        dificultad = ConfigurationManager.getDificultad();
		preferencias = Gdx.app.getPreferences("Scores");

		LOADING = new LoadingScreen(this);

		manager.load("data/Loading.png", Texture.class);
		manager.load("data/Background.png", Texture.class);
		manager.load("data/GameOver.png", Texture.class);
		manager.load("data/Records.png", Texture.class);
		manager.load("data/BotonPlay.png", Texture.class);
		manager.load("data/BotonExit.png", Texture.class);
		manager.load("data/BotonRecords.png", Texture.class);
		manager.load("data/configuration.png",Texture.class);
		manager.load("data/Win.png", Texture.class);
		manager.load("data/alien1.png", Texture.class);
        manager.load("data/alien1l.png", Texture.class);
        manager.load("data/alien1r.png", Texture.class);

		manager.load("data/alien2.png", Texture.class);
        manager.load("data/alien2l.png", Texture.class);
        manager.load("data/alien2r.png", Texture.class);

		manager.load("data/alien3.png", Texture.class);
        manager.load("data/alien3l.png", Texture.class);
        manager.load("data/alien3r.png", Texture.class);

        manager.load("data/alien4.png", Texture.class);
        manager.load("data/alien4l.png", Texture.class);
        manager.load("data/alien4r.png", Texture.class);

		manager.load("data/naveAlienBonus.png", Texture.class);
		manager.load("data/ship.png", Texture.class);
		manager.load("data/shot.png", Texture.class);
		manager.load("data/shotAlien.png", Texture.class);
		manager.load("data/balaza.png", Texture.class);
		manager.load("data/bolaBalaEspecial.png", Texture.class);
		manager.load("data/explosion.wav", Sound.class);
		manager.load("data/shot.wav", Sound.class);
        for (int i = 1; i <= 17; i++) {
            manager.load("animated/explosion-" + i + ".png",Texture.class);
        }
		setScreen(LOADING);
	}

	public void cargarPantallas() {
		PRINCIPAL = new MainScreen(this);
		JUEGO = new GameScreen(this);
		GAMEOVER = new GameOverScreen(this);
		WIN = new WinScreen(this);
		RECORDS = new RecordScreen(this);
		CONFIGURACION = new ConfigurationScreen(this);
	}

	public SpriteBatch getBatch() {
		return batch;
	}

	public AssetManager getManager() {
		return manager;
	}

	public BitmapFont getFont() {
		return font;
	}

	public Preferences getPreferencias() {
		return preferencias;
	}

	@Override
	public void dispose() {
		super.dispose();
		manager.dispose();
		batch.dispose();
	}

	@Override
	public void render() {
		super.render();
	}
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}
	@Override
	public void pause() {
		super.pause();
	}
	@Override
	public void resume() {
		super.resume();
	}
}
