package com.alberto.game.charapters;

public class Explosion {
    private final float intervalo = 1f / 17f;

    private float acumulado = 0;
    private int estagio = 1;
    private float x;
    private float y;

    public void atualizar(float delta) {
        acumulado += delta;
        if (acumulado >= intervalo) {
            acumulado = 0;
            estagio = estagio + 1;
        }
    }

    public int getEstado() {
        return estagio;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
