package com.alberto.game.charapters;

import com.alberto.game.manager.ConfigurationManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.alberto.game.charapters.Shot;

public class Alien {
	public final static int IZQUIERDA = -1;
	public final static int DERECHA = 1;
	private final float DES_SPEED = 10;
	
	private Texture texturaAlien;
	private Vector2 posicion;
	private float anchura, altura;
	private Rectangle bordes;
	private int movimiento;
	private float margenIzquierdo;
	private float margenDerecho;
	private float velocidad;
	private int contadorDeBajadas;

	public Alien(Texture texturaAlien, Vector2 posicion, float margenIzquierdo, float margenDerecho, float velocidad) {
		this.texturaAlien = texturaAlien;
		this.posicion = posicion;
		this.anchura = texturaAlien.getWidth();
		this.altura = texturaAlien.getHeight();
		bordes = new Rectangle(posicion.x, posicion.y, anchura, altura);
		movimiento = DERECHA;
		this.margenIzquierdo = margenIzquierdo;
		this.margenDerecho = margenDerecho;
		this.velocidad = velocidad;
		contadorDeBajadas = 0;
	}
	
	public void draw(SpriteBatch batch) {
		batch.draw(texturaAlien, posicion.x, posicion.y, anchura, altura);
	}
	
	public void update(float delta) {
		if(movimiento == DERECHA) {
			posicion.x = posicion.x + velocidad;
			if(posicion.x + anchura > margenDerecho) {
				movimiento = IZQUIERDA;
				posicion.y = posicion.y - DES_SPEED * delta * ConfigurationManager.getDificultad();
				contadorDeBajadas += 1;

				
			}
		}
		if(movimiento == IZQUIERDA) {
			posicion.x = posicion.x - velocidad;
			if(posicion.x < margenIzquierdo) {
				movimiento = DERECHA;
				posicion.y = posicion.y - DES_SPEED * delta * ConfigurationManager.getDificultad();
				contadorDeBajadas += 1;
			}
		}

		bordes.x = posicion.x;
		bordes.y = posicion.y;
	}

    public void setTexturaAlien(Texture texturaAlien) {
        this.texturaAlien = texturaAlien;
    }

    public boolean Muerto(Shot disparo) {
		return colisiona(bordes, disparo.getBordes());
	}
	
	private boolean colisiona(Rectangle a, Rectangle b) {
		return a.overlaps(b);
	}
	

	public Vector2 getPosicion() {
		return posicion;
	}

	public int getContadorDeBajadas() {
		return contadorDeBajadas;
	}

	public float getAnchura() {
		return anchura;
	}

	public float getAltura() {
		return altura;
	}

	public Rectangle getBordes() {
		return bordes;
	}

    public int getMovimiento() {
        return movimiento;
    }
}
