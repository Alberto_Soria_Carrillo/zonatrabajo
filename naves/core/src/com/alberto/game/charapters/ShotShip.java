package com.alberto.game.charapters;

import com.alberto.game.MainGame;
import com.alberto.game.manager.ConfigurationManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.alberto.game.charapters.Shot;

public class ShotShip extends Shot {
	private static final float SPEED = 550;
	
	public ShotShip(MainGame invaders, Vector2 posicion) {
		super(invaders, posicion);
		texturaShot = invaders.getManager().get("data/shot.png", Texture.class);
		this.anchura = texturaShot.getWidth();
		this.altura = texturaShot.getHeight();
		bordes = new Rectangle(posicion.x, posicion.y, anchura, altura);
	}

	public void update(float delta) {
		posicion.y = posicion.y + SPEED * delta;
		bordes.y = posicion.y;
	}
	
	public void alienMuerto() {
		posicion.y = Gdx.graphics.getHeight();
		if(ConfigurationManager.haySonido())
		    explosion.play();
	}
}
