package com.alberto.game.charapters;

import com.alberto.game.MainGame;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.alberto.game.charapters.Ship;

public class Ball {
	private static final float SPEED = 100;

	private MainGame invaders;
	private Texture texturaBola;
	private Vector2 posicion;
	private float anchura, altura;
	private Rectangle bordes;
	
	public Ball(MainGame invaders, Vector2 posicion) {
		this.invaders = invaders;
		this.posicion = posicion;
		texturaBola = invaders.getManager().get("data/bolaBalaEspecial.png", Texture.class);
		anchura = texturaBola.getWidth();
		altura = texturaBola.getHeight();
		bordes = new Rectangle(posicion.x, posicion.y, anchura, altura);
	}
	
	public void draw(SpriteBatch batch) {
		batch.draw(texturaBola, posicion.x, posicion.y, anchura, altura);
	}
	
	public void update(float delta) {
		posicion.y = posicion.y - SPEED*delta;
		bordes.x = posicion.x;
		bordes.y = posicion.y;
	}
	
	public boolean colisionConNave(Ship nave) {
		return bordes.overlaps(nave.getBordes());
	}
	
	public void setPosicion(float x, float y) {
		posicion.x = x;
		posicion.y = y;
	}
	

	public Vector2 getPosicion() {
		return posicion;
	}

	public float getAnchura() {
		return anchura;
	}

	public float getAltura() {
		return altura;
	}

	public Rectangle getBordes() {
		return bordes;
	}
}
