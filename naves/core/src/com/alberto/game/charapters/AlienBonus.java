package com.alberto.game.charapters;

import com.alberto.game.MainGame;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.alberto.game.charapters.Shot;


public class AlienBonus {
	private MainGame invaders;
	private Texture texturaAlienBonus;
	private Vector2 posicion;
	private float anchura, altura;
	private Rectangle bordes;
	private float velocidad;
	
	public AlienBonus(MainGame invaders, Vector2 posicion, float velocidad) {
		this.posicion = posicion;
		this.invaders = invaders;
		texturaAlienBonus = invaders.getManager().get("data/naveAlienBonus.png", Texture.class);
		this.anchura = texturaAlienBonus.getWidth();
		this.altura = texturaAlienBonus.getHeight();
		bordes = new Rectangle(posicion.x, posicion.y, anchura, altura);
		this.velocidad = velocidad;
	}
	
	public void draw(SpriteBatch batch) { // Dibuja la nave
		batch.draw(texturaAlienBonus, posicion.x, posicion.y, anchura, altura);
	}
	
	public void update() {
		posicion.x = posicion.x + velocidad;
		bordes.x = posicion.x;
	}
	
	public boolean muerto(Shot disparo) {
		return colisiona(bordes, disparo.getBordes());
	}
	
	private boolean colisiona(Rectangle a, Rectangle b) {
		return a.overlaps(b);
	}
	

		public Vector2 getPosicion() {
			return posicion;
		}

		public float getAnchura() {
			return anchura;
		}

		public float getAltura() {
			return altura;
		}

		public Rectangle getBordes() {
			return bordes;
		}
}
