package com.alberto.game.charapters;

import com.badlogic.gdx.Application.ApplicationType;
import com.alberto.game.MainGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.alberto.game.charapters.Shot;

public class Ship {
	private static final float SPEED = 20;
	private static final float MARGEN_IZQUIERDO = 10;
	private static final float MARGEN_DERECHO = Gdx.graphics.getWidth() - 10;
	
	private Texture texturaShip;
	private Vector2 posicion;
	private float anchura, altura;
	private Rectangle bordes;
		
	public Ship(MainGame invaders, Vector2 posicion) {
		texturaShip = invaders.getManager().get("data/ship.png", Texture.class);
		texturaShip.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		this.posicion = posicion;
		this.anchura = texturaShip.getWidth();
		this.altura = texturaShip.getHeight();
		bordes = new Rectangle(posicion.x, posicion.y, anchura, altura);
	}
	
	public void draw(SpriteBatch batch) { // Dibuja la nave
		batch.draw(texturaShip, posicion.x, posicion.y, anchura, altura);
	}
	
	public boolean update() {
		boolean disparo = false;
		
		if(Gdx.app.getType() == ApplicationType.Desktop)
			disparo = entradaDesktop();
		else if(Gdx.app.getType() == ApplicationType.Android)
			disparo = entradaAndroid();
			
		bordes.x = posicion.x;
		bordes.y = posicion.y;
		
		return disparo;
	}
	
	private boolean entradaDesktop() {
		if(Gdx.input.isKeyPressed(Keys.RIGHT) && noChoqueBordeDerecha())
			posicion.x = posicion.x + SPEED;
		if(Gdx.input.isKeyPressed(Keys.LEFT) && noChoqueBordeIzquierda())
			posicion.x = posicion.x - SPEED;
		if(Gdx.input.isKeyPressed(Keys.SPACE))
			return true;
		else 
			return false;
	}
	private boolean entradaAndroid() {
		if(Gdx.input.isTouched()) {
			if(Gdx.input.getX() < MARGEN_IZQUIERDO)
				posicion.x = MARGEN_IZQUIERDO;
			else if(Gdx.input.getX() + anchura > MARGEN_DERECHO)
				posicion.x = MARGEN_DERECHO - anchura;
			else
				posicion.x = Gdx.input.getX() - (anchura / 2);
		}

		if(Gdx.input.isTouched(1))
			return true;
		else
			return false;
	}
	
	private boolean noChoqueBordeDerecha() {
		return posicion.x + anchura < MARGEN_DERECHO;
	}
	private boolean noChoqueBordeIzquierda() {
		return posicion.x > MARGEN_IZQUIERDO;
	}
	
	public boolean tocadoPorDisparo(Shot disparo) {
		return colisiona(bordes, disparo.getBordes());
	}
	
	private boolean colisiona(Rectangle a, Rectangle b) {
		return a.overlaps(b);
	}
	
	public void volverAPosicionInicial() {
		posicion.x = 10;
		posicion.y = 10;
	}
		

	public static float getSpeed() {
		return SPEED;
	}

	public Vector2 getPosicion() {
		return posicion;
	}

	public float getAnchura() {
		return anchura;
	}

	public float getAltura() {
		return altura;
	}

	public Rectangle getBordes() {
		return bordes;
	}
}
