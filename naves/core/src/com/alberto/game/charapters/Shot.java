package com.alberto.game.charapters;

import com.alberto.game.MainGame;
import com.alberto.game.manager.ConfigurationManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;


public abstract class Shot {
	protected MainGame invaders;
	protected Texture texturaShot;
	protected Sound shotSound, explosion;
	protected Vector2 posicion;
	protected float anchura, altura;
	protected Rectangle bordes;
	
	public Shot(MainGame invaders, Vector2 posicion) {
		this.invaders = invaders;
		shotSound = invaders.getManager().get("data/shot.wav", Sound.class);
		explosion = invaders.getManager().get("data/explosion.wav", Sound.class);
		this.posicion = posicion;
	}
	
	public void draw(SpriteBatch batch) {
		batch.draw(texturaShot, posicion.x, posicion.y, anchura, altura);
	}
	
	public abstract void update(float delta);

	public void disparoSonido() {
        if(com.alberto.game.manager.ConfigurationManager.haySonido())
		    shotSound.play();
	}
	
	public abstract void alienMuerto();
	
	public void naveTocada() {
		posicion.y = -100;
		if(ConfigurationManager.haySonido())
            explosion.play();
	}
	

	public Vector2 getPosicion() {
		return posicion;
	}

	public float getAnchura() {
		return anchura;
	}

	public float getAltura() {
		return altura;
	}

	public Rectangle getBordes() {
		return bordes;
	}
}
