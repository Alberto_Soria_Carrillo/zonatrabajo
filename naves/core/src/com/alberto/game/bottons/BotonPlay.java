package com.alberto.game.bottons;

import com.alberto.game.MainGame;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.alberto.game.bottons.Boton;

public class BotonPlay extends Boton {

	public BotonPlay(MainGame mainGame, Vector2 posicion) {
		super(mainGame, posicion);
		textura = mainGame.getManager().get("data/BotonPlay.png", Texture.class);
		asignarBordes();
	}
	
	@Override
	protected void funcionamiento() {
		invaders.setScreen(invaders.JUEGO);
	}
}
