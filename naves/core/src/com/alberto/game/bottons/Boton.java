package com.alberto.game.bottons;

import com.alberto.game.MainGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;


public abstract class Boton {
	protected MainGame invaders;
	protected Vector2 posicion;
	protected Rectangle bordes; 
	protected float xMinima;
	protected float yMinima;
	protected float xMaxima;
	protected float yMaxima;
	protected static int tiempoPulsacion;

	protected Texture textura;
	
	public Boton(MainGame mainGame, Vector2 posicion) {
		this.invaders = mainGame;
		this.posicion = posicion;
		tiempoPulsacion = 0;
	}
	
	protected void asignarBordes() {
		bordes = new Rectangle(posicion.x, posicion.y, textura.getWidth(), textura.getHeight());
	
		xMinima = posicion.x;
		yMaxima = Gdx.graphics.getHeight() - posicion.y;
		xMaxima = posicion.x + bordes.width;
		yMinima = Gdx.graphics.getHeight() - (posicion.y + bordes.height);
	}
	
	public void draw(SpriteBatch batch) {
		batch.draw(textura, posicion.x, posicion.y, bordes.width, bordes.height);
	}
	
	public void update() {
		if(sePulsaElBoton() && tiempoPulsacion == 0) {
			tiempoPulsacion = 150;
			funcionamiento();
		}
		else if(tiempoPulsacion > 0)
			tiempoPulsacion--;
	}
	private boolean sePulsaElBoton() {
		return Gdx.input.isTouched() && Gdx.input.getX() >= xMinima && Gdx.input.getX() <= xMaxima
				&& Gdx.input.getY() >= yMinima && Gdx.input.getY() <= yMaxima;
	}
	
	protected abstract void funcionamiento();


	public Vector2 getPosicion() {
		return posicion;
	}
	
	public void setPosicion(Vector2 posicion) {
		this.posicion = posicion;
	}

	public Rectangle getBordes() {
		return bordes;
	}

	public void setBordes(Rectangle bordes) {
		this.bordes = bordes;
	}
}
